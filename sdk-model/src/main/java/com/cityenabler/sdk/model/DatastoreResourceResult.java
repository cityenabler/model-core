package com.cityenabler.sdk.model;

import java.util.List;
import java.util.Map;

public class DatastoreResourceResult {
	private Boolean include_total;
	private String resource_id;
	private List<DatastoreResourceResultField> fields;
	private String records_format;
	private List<Map<String, String>> records;
	private Integer limit;
	private Integer offset;
	private DatastoreResourceResultLinks _links;
	private Integer total;
	
	public Boolean getInclude_total() {
		return include_total;
	}
	public void setInclude_total(Boolean include_total) {
		this.include_total = include_total;
	}
	public String getResource_id() {
		return resource_id;
	}
	public void setResource_id(String resource_id) {
		this.resource_id = resource_id;
	}
	public List<DatastoreResourceResultField> getFields() {
		return fields;
	}
	public void setFields(List<DatastoreResourceResultField> fields) {
		this.fields = fields;
	}
	public String getRecords_format() {
		return records_format;
	}
	public void setRecords_format(String records_format) {
		this.records_format = records_format;
	}
	public List<Map<String, String>> getRecords() {
		return records;
	}
	public void setRecords(List<Map<String, String>> records) {
		this.records = records;
	}
	public DatastoreResourceResultLinks get_links() {
		return _links;
	}
	public void setLinks(DatastoreResourceResultLinks _links) {
		this._links = _links;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getOffset() {
		return offset;
	}
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	
}
