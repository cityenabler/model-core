package com.cityenabler.sdk.model.fiwaredatamodel;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.cityenabler.sdk.model.NGSIAttribute;
import com.cityenabler.sdk.model.NGSIEntity;
import com.cityenabler.sdk.model.type.GeoJSON;

public class Vehicle extends NGSIEntity{
	/** 
	 * Last update timestamp of this entity.
	 * @AttributeType DateTime
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateModified;
	/**
	 * Entity's creation timestamp.
	 * @AttributeType DateTime 
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateCreated;
	/**
	 * Name given to this vehicle.
	 * @NormativeReferences  https://schema.org/name
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> name;
	/**
	 * Vehicle description.
	 * @NormativeReferences  https://schema.org/description
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> description;
	/**
	 * Type of vehicle from the point of view of its structural characteristics. This is different than the vehicle category (see below).
	 * @AttributeType Text
	 * @AllowedValues The following values defined by VehicleTypeEnum and VehicleTypeEnum2, DATEX 2 version 2.3: (agriculturalVehicle, bicycle, bus, minibus, car, caravan, tram, tanker,  carWithCaravan, carWithTrailer, lorry, moped, tanker,  motorcycle,  motorcycleWithSideCar, motorscooter, trailer, van, caravan, constructionOrMaintenanceVehicle)
	 * (trolley, binTrolley, sweepingMachine, cleaningTrolley)
	 * @Mandatory
	 */
	private NGSIAttribute<String> vehicleType;
	/**
	 * Vehicle category(ies) from an external point of view. This is different than the vehicle type (car, lorry, etc.) represented by the vehicleType property.
	 * @AttributeType List of Text
	 * @AllowedValues (public, private, municipalServices, specialUsage).(tracked, nonTracked). Tracked vehicles are those vehicles which position is permanently tracked by a remote system.
	 * Or any other needed by an application They incorporate a GPS receiver together with a network connection to periodically update a reported position (location, speed, heading ...).
	 * @Mandatory
	 */
	private NGSIAttribute<List<String>> category;
	/**
	 * Vehicle's last known location represented by a GeoJSON Point. Such point may contain the vehicle's altitude as the third component of the coordinates array.
	 * @AttributeType geo:json
	 * @NormativeReferences https://tools.ietf.org/html/rfc7946
	 * @Mandatory only if category contains tracked
	 */
	private Optional<NGSIAttribute<? extends GeoJSON<Object>>> location;
	/**
	 * Vehicle's previous location represented by a GeoJSON Point. Such point may contain the previous vehicle's altitude as the third component of thecoordinates array.
	 * @AttributeType geo:json
	 * @NormativeReferences https://tools.ietf.org/html/rfc7946
	 * @Optional
	 */
	private Optional<NGSIAttribute<? extends GeoJSON<Object>>> previousLocation;
	/**
	 * Denotes the magnitude of the horizontal component of the vehicle's current velocity and is specified in Kilometers per Hour. If provided, the value of the speed attribute must be a non-negative real number. null MAY be used if speed is transiently unknown for some reason.
	 * @AttributeType Number
	 * @DefaultUnit Kilometers per hour
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> speed;
	/**
	 * Denotes the direction of travel of the vehicle and is specified in decimal degrees, where 0° ≤ heading < 360°, counting clockwise relative to the true north. 
	 * If the vehicle is stationary (i.e. the value of the speed attribute is 0), then the value of the heading attribute must be equal to null. 
	 * null MAY be used if heading is transiently unknown for some reason.
	 * @AttributeType Number
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> heading;
	/**
	 * Current weight of the vehicle's cargo.
	 * @AttributeType Number
	 * @DefaultUnit Kilograms
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> cargoWeight;
	/**
	 * The Vehicle Identification Number (VIN) is a unique serial number used by the automotive industry to identify individual motor vehicles.
	 * @NormativeReferences https://schema.org/vehicleIdentificationNumber
	 * @Mandatory if neither vehiclePlateIdentifier nor fleetVehicleId is defined.
	 */
	private Optional<NGSIAttribute<String>> vehicleIdentificationNumber;
	/**
	 * An identifier or code displayed on a vehicle registration plate attached to the vehicle used for official identification purposes. The registration identifier is numeric or alphanumeric and is unique within the issuing authority's region.
	 * @NormativeReferences DATEX II vehicleRegistrationPlateIdentifier
	 * @AttributeType Text
	 * @Mandatory  if neither vehicleIdentificationNumber nor fleetVehicleId is defined.
	 */
	private Optional<NGSIAttribute<String>> vehiclePlateIdentifier;
	/**
	 * The identifier of the vehicle in the context of the fleet of vehicles to which it belongs.
	 * @AttributeType Text
	 * @Mandatyory if neither vehiclePlateIdentifier nor vehicleIdentificationNumber is defined.
	 */
	private Optional<NGSIAttribute<String>> fleetVehicleId;
	/**
	 * The date of the first registration of the vehicle with the respective public authorities
	 * @NormativeReferences The date of the first registration of the vehicle with the respective public authorities
	 * @Optional
	 */
	private Optional<NGSIAttribute<Date>> dateVehicleFirstRegistered;
	/**
	 * Timestamp which denotes when the vehicle was first used
	 * @AttributeType DateTime
	 * @Optional
	 */
	private Optional<NGSIAttribute<Date>> dateFirstUsed;
	/**
	 * The date the item e.g. vehicle was purchased by the current owner.
	 * @NormativeReferences https://schema.org/purchaseDate
	 * @Optional
	 */
	private Optional<NGSIAttribute<Date>> purchaseDate;
	/**
	 * A short text indicating the configuration of the vehicle, e.g. '5dr hatchback ST 2.5 MT 225 hp' or 'limited edition'.
	 * @NormativeReferences https://schema.org/vehicleConfiguration
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> vehicleConfiguration;
	/**
	 * Vehicle's color.
	 * @NormativeReferences https://schema.org/color
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> color;
	/**
	 * Vehicle's owner.
	 * @AttributeType  https://schema.org/Person
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> owner;
	/**
	 * Feature(s) incorporated by the vehicle.
	 * @AttributeType List of Text
	 * @AllowedValues  (gps, airbag, overspeed, abs, wifi, backCamera, proximitySensor, disabledRamp, alarm, internetConnection) or any other needed by the application.
	 * In order to represent multiple instances of a feature it can be used the following syntax: "<feature>,<occurences>". For example, a car with 4 airbags will be represented by "airbag,4".
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> feature;
	/**
	 * Service(s) the vehicle is capable of providing or it is assigned to.
	 * @AttributeType List of Text
	 * @AllowedValues  (garbageCollection, parksAndGardens, construction, streetLighting, roadSignalling, cargoTransport, urbanTransit, maintenance, streetCleaning,  wasteContainerCleaning, auxiliaryServices goodsSelling,  fairground, specialTransport) or any other value needed by an specific application.
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> serviceProvided;
	/**
	 * Indicates whether the vehicle is been used for special purposes, like commercial rental, driving school, or as a taxi. The legislation in many countries requires this information to be revealed when offering a car for sale.
	 * @NormativerReferences https://auto.schema.org/vehicleSpecialUsage
	 * @AllowedValues (taxi, ambulance, police, fireBrigade, schoolTransportation, military)
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> vehicleSpecialUsage;
	/**
	 * Vehicle's model.
	 * @AttributeType Reference to a VehicleModel entity.
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> refVehicleModel;
	/**
	 * Higher level area served by this vehicle. It can be used to group vehicles per responsible, district, neighbourhood, etc.
	 * @AttributeType Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> areaServed;
	/**
	 *  Vehicle status (from the point of view of the service provided, so it could not apply to private vehicles).
	 * @AttributeType Text
	 * @AllowedValues parked : Vehicle is parked and not providing any service at the moment.
	 * onRoute : Vehicle is performing a mission. A comma-separated modifier(s) can be added to indicate what mission is currently delivering the vehicle. For instance "onRoute,garbageCollection" can be used to denote that the vehicle is on route and in a garbage collection mission.
	 * broken : Vehicle is suffering a temporary breakdown.
	 * outOfService : Vehicle is on the road but not performing any mission, probably going to its parking area.
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> serviceStatus;
	
	public Vehicle() {
		super();
	}
	
	public Vehicle(String id) {
		super(id);
	}
	
	public NGSIAttribute<Date> getDateModified() {
		return dateModified;
	}
	public void setDateModified(NGSIAttribute<Date> dateModified) {
		this.dateModified = dateModified;
	}
	public NGSIAttribute<Date> getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(NGSIAttribute<Date> dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Optional<NGSIAttribute<String>> getName() {
		return name;
	}
	public void setName(NGSIAttribute<String> name) {
		this.name = Optional.of(name);
	}
	public Optional<NGSIAttribute<String>> getDescription() {
		return description;
	}
	public void setDescription(NGSIAttribute<String> description) {
		this.description = Optional.of(description);
	}
	public NGSIAttribute<String> getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(NGSIAttribute<String> vehicleType) {
		this.vehicleType = vehicleType;
	}
	public NGSIAttribute<List<String>> getCategory() {
		return category;
	}
	public void setCategory(NGSIAttribute<List<String>> category) {
		this.category = category;
	}
	public Optional<NGSIAttribute<? extends GeoJSON<Object>>> getLocation() {
		return location;
	}
	public void setLocation(NGSIAttribute<? extends GeoJSON<Object>> location) {
		this.location = Optional.of(location);
	}
	public Optional<NGSIAttribute<? extends GeoJSON<Object>>> getPreviousLocation() {
		return previousLocation;
	}
	public void setPreviousLocation(NGSIAttribute<? extends GeoJSON<Object>> previousLocation) {
		this.previousLocation = Optional.of(previousLocation);
	}
	public Optional<NGSIAttribute<Double>> getSpeed() {
		return speed;
	}
	public void setSpeed(NGSIAttribute<Double> speed) {
		this.speed = Optional.of(speed);
	}
	public Optional<NGSIAttribute<Double>> getHeading() {
		return heading;
	}
	public void setHeading(NGSIAttribute<Double> heading) {
		this.heading = Optional.of(heading);
	}
	public Optional<NGSIAttribute<Double>> getCargoWeight() {
		return cargoWeight;
	}
	public void setCargoWeight(NGSIAttribute<Double> cargoWeight) {
		this.cargoWeight = Optional.of(cargoWeight);
	}
	public Optional<NGSIAttribute<String>> getVehicleIdentificationNumber() {
		return vehicleIdentificationNumber;
	}
	public void setVehicleIdentificationNumber(NGSIAttribute<String> vehicleIdentificationNumber) {
		this.vehicleIdentificationNumber = Optional.of(vehicleIdentificationNumber);
	}
	public Optional<NGSIAttribute<String>> getVehiclePlateIdentifier() {
		return vehiclePlateIdentifier;
	}
	public void setVehiclePlateIdentifier(NGSIAttribute<String> vehiclePlateIdentifier) {
		this.vehiclePlateIdentifier = Optional.of(vehiclePlateIdentifier);
	}
	public Optional<NGSIAttribute<String>> getFleetVehicleId() {
		return fleetVehicleId;
	}
	public void setFleetVehicleId(NGSIAttribute<String> fleetVehicleId) {
		this.fleetVehicleId = Optional.of(fleetVehicleId);
	}
	public Optional<NGSIAttribute<Date>> getDateVehicleFirstRegistered() {
		return dateVehicleFirstRegistered;
	}
	public void setDateVehicleFirstRegistered(NGSIAttribute<Date> dateVehicleFirstRegistered) {
		this.dateVehicleFirstRegistered = Optional.of(dateVehicleFirstRegistered);
	}
	public Optional<NGSIAttribute<Date>> getDateFirstUsed() {
		return dateFirstUsed;
	}
	public void setDateFirstUsed(NGSIAttribute<Date> dateFirstUsed) {
		this.dateFirstUsed = Optional.of(dateFirstUsed);
	}
	public Optional<NGSIAttribute<Date>> getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(NGSIAttribute<Date> purchaseDate) {
		this.purchaseDate = Optional.of(purchaseDate);
	}
	public Optional<NGSIAttribute<String>> getVehicleConfiguration() {
		return vehicleConfiguration;
	}
	public void setVehicleConfiguration(NGSIAttribute<String> vehicleConfiguration) {
		this.vehicleConfiguration = Optional.of(vehicleConfiguration);
	}
	public Optional<NGSIAttribute<String>> getColor() {
		return color;
	}
	public void setColor(NGSIAttribute<String> color) {
		this.color = Optional.of(color);
	}
	public Optional<NGSIAttribute<String>> getOwner() {
		return owner;
	}
	public void setOwner(NGSIAttribute<String> owner) {
		this.owner = Optional.of(owner);
	}
	public Optional<NGSIAttribute<List<String>>> getFeature() {
		return feature;
	}
	public void setFeature(NGSIAttribute<List<String>> feature) {
		this.feature = Optional.of(feature);
	}
	public Optional<NGSIAttribute<List<String>>> getServiceProvided() {
		return serviceProvided;
	}
	public void setServiceProvided(NGSIAttribute<List<String>> serviceProvided) {
		this.serviceProvided = Optional.of(serviceProvided);
	}
	public Optional<NGSIAttribute<String>> getVehicleSpecialUsage() {
		return vehicleSpecialUsage;
	}
	public void setVehicleSpecialUsage(NGSIAttribute<String> vehicleSpecialUsage) {
		this.vehicleSpecialUsage = Optional.of(vehicleSpecialUsage);
	}
	public Optional<NGSIAttribute<String>> getRefVehicleModel() {
		return refVehicleModel;
	}
	public void setRefVehicleModel(NGSIAttribute<String> refVehicleModel) {
		this.refVehicleModel = Optional.of(refVehicleModel);
	}
	public Optional<NGSIAttribute<String>> getAreaServed() {
		return areaServed;
	}
	public void setAreaServed(NGSIAttribute<String> areaServed) {
		this.areaServed = Optional.of(areaServed);
	}
	public Optional<NGSIAttribute<String>> getServiceStatus() {
		return serviceStatus;
	}
	public void setServiceStatus(NGSIAttribute<String> serviceStatus) {
		this.serviceStatus = Optional.of(serviceStatus);
	}
	
}
