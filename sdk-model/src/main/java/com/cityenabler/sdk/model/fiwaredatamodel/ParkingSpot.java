package com.cityenabler.sdk.model.fiwaredatamodel;

import java.util.Date;
import java.util.Optional;

import com.cityenabler.sdk.model.NGSIAttribute;
import com.cityenabler.sdk.model.NGSIEntity;
import com.cityenabler.sdk.model.type.GeoJSON;

public class ParkingSpot extends NGSIEntity {
	/** 
	 * Last update timestamp of this entity.
	 * @AttributeType DateTime
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateModified;
	/**
	 * Entity's creation timestamp.
	 * @AttributeType DateTime 
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateCreated;
	/**
	 * Name of this parking spot. It can denote the number or label used to identify it within a parking site.
	 * @NormativeReferences https://schema.org/name
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> name;
	/**
	 * Description about the parking spot.
	 * @NormativeReferences https://schema.org/description
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> description;
	/**
	 * Geolocation of the parking spot, represented by a GeoJSON Point.
	 * @AttributeType geo:json
	 * @NormativeReferences https://tools.ietf.org/html/rfc7946
	 * @Mandatory If "address" is not defined
	 */
	private Optional<NGSIAttribute<? extends GeoJSON<Object>>> location;
	/**
	 * Registered parking spot civic address.
	 * @NormativeReferences https://schema.org/address
	 * @Mandatory If "location" is not defined
	 */
	private Optional<NGSIAttribute<String>> address;
	/**
	 * Status of the parking spot from the point of view of occupancy.
	 * @AttributeType Text
	 * @AllowedValues one Of (occupied, free, closed, unknown)
	 * @Mandatory
	 */
	private Optional<NGSIAttribute<String>> status;
	/**
	 * Width of the parking spot.
	 * @AttributeType Number
	 * @DefaultUnit Meters
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> width;
	/**
	 * Length of the parking spot.
	 * @AttributeType Number
	 * @DefaultUnit Meters
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> length;
	/**
	 * Group to which the parking spot belongs to. For model simplification purposes only one group is allowed per parking spot.
	 * @AttributeType Reference to an entity of type ParkingGroup
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> refParkingGroup ;
	/**
	 * Parking site to which the the parking spot belongs to.
	 * @AttributeType Reference to an entity of type OnStreetParking or type OffStreetParking, depending on the value of the category attribute
	 * @Mandatory
	 */
	private NGSIAttribute<String> refParkingSite;
	/**
	 * Category(ies) of the parking spot.
	 * @AttributeType Text
	 * @AllowedValues
	 * onstreet : The parking spot belongs to an onstreet parking site.
	 * offstreet : The parking spot belongs to an onstreet parking site.
	 * Other values as per application needs
	 * @Mandatory
	 */
	private NGSIAttribute<String> category;
	/**
	 * Timestamp saved by FIWARE's IoT Agent. Note: This attribute has not been harmonized to keep backwards compatibility with current FIWARE reference implementations.
	 * @AttributeType DateTime
	 * @Optional
	 */
	private Optional<NGSIAttribute<Date>> TimeIstant;
	
	public ParkingSpot() {
		super();
	}
	
	public ParkingSpot(String id) {
		super(id);
	}
	
	public NGSIAttribute<Date> getDateModified() {
		return dateModified;
	}
	public void setDateModified(NGSIAttribute<Date> dateModified) {
		this.dateModified = dateModified;
	}
	public NGSIAttribute<Date> getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(NGSIAttribute<Date> dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Optional<NGSIAttribute<String>> getName() {
		return name;
	}
	public void setName(NGSIAttribute<String> name) {
		this.name = Optional.of(name);
	}
	public Optional<NGSIAttribute<String>> getDescription() {
		return description;
	}
	public void setDescription(NGSIAttribute<String> description) {
		this.description = Optional.of(description);
	}
	public Optional<NGSIAttribute<? extends GeoJSON<Object>>> getLocation() {
		return location;
	}
	public void setLocation(NGSIAttribute<? extends GeoJSON<Object>> location) {
		this.location = Optional.of(location);
	}
	public Optional<NGSIAttribute<String>> getAddress() {
		return address;
	}
	public void setAddress(NGSIAttribute<String> address) {
		this.address = Optional.of(address);
	}
	public Optional<NGSIAttribute<String>> getStatus() {
		return status;
	}
	public void setStatus(NGSIAttribute<String> status) {
		this.status = Optional.of(status);
	}
	public Optional<NGSIAttribute<Double>> getWidth() {
		return width;
	}
	public void setWidth(NGSIAttribute<Double> width) {
		this.width = Optional.of(width);
	}
	public Optional<NGSIAttribute<Double>> getLength() {
		return length;
	}
	public void setLength(NGSIAttribute<Double> length) {
		this.length = Optional.of(length);
	}
	public Optional<NGSIAttribute<String>> getRefParkingGroup() {
		return refParkingGroup;
	}
	public void setRefParkingGroup(NGSIAttribute<String> refParkingGroup) {
		this.refParkingGroup = Optional.of(refParkingGroup);
	}
	public NGSIAttribute<String> getRefParkingSite() {
		return refParkingSite;
	}
	public void setRefParkingSite(NGSIAttribute<String> refParkingSite) {
		this.refParkingSite = refParkingSite;
	}
	public NGSIAttribute<String> getCategory() {
		return category;
	}
	public void setCategory(NGSIAttribute<String> category) {
		this.category = category;
	}
	public Optional<NGSIAttribute<Date>> getTimeIstant() {
		return TimeIstant;
	}
	public void setTimeIstant(NGSIAttribute<Date> timeIstant) {
		TimeIstant = Optional.of(timeIstant);
	}
	
	
	
}
