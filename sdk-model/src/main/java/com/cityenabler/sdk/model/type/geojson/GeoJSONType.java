package com.cityenabler.sdk.model.type.geojson;

public enum GeoJSONType {
	Point("Point"),
	Polygon("Polygon"),
	Multipolygon("Multipolygon");
	
	private String typeValue;
	
	private GeoJSONType(String typeValue) {
		this.typeValue = typeValue;
	}
	
	public String getTypeValue() {
		return typeValue;
	}
	
	@Override
	public String toString() {
		return typeValue;
	}
}
