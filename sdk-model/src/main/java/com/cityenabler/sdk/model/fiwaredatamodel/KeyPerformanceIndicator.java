package com.cityenabler.sdk.model.fiwaredatamodel;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.cityenabler.sdk.model.NGSIAttribute;
import com.cityenabler.sdk.model.NGSIEntity;
import com.cityenabler.sdk.model.type.GeoJSON;
import com.cityenabler.sdk.model.type.AggregatedData;
import com.cityenabler.sdk.model.type.CalculationPeriod;

public class KeyPerformanceIndicator extends NGSIEntity {
	/**
	 * Name of this point of interest.
	 * @NormativeReferences https://schema.org/name
	 * @Optional
	 */
	private NGSIAttribute<String> name;
	/**
	 * Alternative name for this point of interest.
	 * @NormativeReferences https://schema.org/alternateName
	 * @Mandatory
	 */
	private Optional<NGSIAttribute<String>> alternateName;
	/**
	 * Subject organization evaluated by the KPI.
	 * @AttributeType Organization
	 * @Mandatory
	 */
	private NGSIAttribute<String> organization;
	/**
	 * Subject process evaluated by the KPI.
	 * @AttributeType Text
	 * @Mandatory Either "process" or "product" must be defined.
	 */
	private NGSIAttribute<String> process;
	/**
	 * Subject product or service evaluated by the KPI.
	 * @AttributeType Product
	 * @Mandatory Either "process" or "product" must be defined.
	 */
	private NGSIAttribute<String> product;
	/**
	 * Provider of the product or service, if any, that this KPI evaluates.
	 * @NormativeReferences  https://schema.org/provider
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> provider;
	/**
	 * For informative purposes, the business target to which this KPI is related to.
	 * @AttributeType Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> businessTarget;
	/**
	 * Indicator's description.
	 * @NormativeReferences https://schema.org/description
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> description;
	/**
	 * How often the KPI is calculated.
	 * @AttributeType Text
	 * @AllowedValues one Of (hourly, daily, weekly, monthly, yearly, quarterly, bimonthly, biweekly) or any other value meaningful for the application and not covered by the above list.
	 * @Mandatory
	 */
	private NGSIAttribute<String> calculationFrequency;
	/**
	 *  Indicator's category.
	 *  @AttributeType List of Text
	 *  @AllowedValues (quantitative, qualitative, leading, lagging, input, process, output, practical, directional, actionable,  financial). 
	 *  Check Wikipedia for a description of each category listed above.
	 *  Any other value meaningful to the application and not covered by the above list.
	 *  @Mandatory
	 */
	private NGSIAttribute<List<String>> category;
	/**
	 * The organization in charge of calculating the KPI.
	 * @AttributeType Organization
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> calculatedBy;
	/**
	 * The calculation method used.
	 * @AttributeType Text
	 * @AllowedValues Allowed values: oneOf ( manual, automatic, semiautomatic). Any other value meaningful to the application and not covered by the above list.
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> calculationMethod;
	/**
	 * For informative purposes, the formula used for calculating the indicator.
	 * @AttributeType Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> calculationFormula;
	/**
	 * Entity(ies) and attribute(s) aggregated by the KPI.
	 * @AttributeType List of StructuredValue
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<AggregatedData>>> aggregatedData;
	/**
	 * KPI's period of time.
	 * @AttributeType StructuredValue
	 * @Optional
	 */
	private Optional<NGSIAttribute<CalculationPeriod>> calculationPeriod; //from e to
	/**
	 * The KPI's current standing as per its kpiValue.
	 * @AttributeType Text
	 * @AllowedValue  one Of (very good, good, fair, bad, very bad)
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> currentStanding;
	/**
	 * @AttributeType Any
	 * @Mandatory
	 */
	private NGSIAttribute<String> kpiValue; //Any value
	/**
	 * The date on which the organization created this KPI. This date might be different than the entity creation date.
	 * @AttributeType DateTime
	 * @Optional
	 */
	private Optional<NGSIAttribute<NGSIAttribute<Date>>> effectiveSince;
	/**
	 * Entity's creation timestamp.
	 * @AttributeType DateTime
	 * @Optional
	 */
	private Optional<NGSIAttribute<NGSIAttribute<Date>>> dateCreated;
	/**
	 * Date on which a new calculation of the KPI should be available.
	 * @AttributeType DateTime
	 * @Optional
	 */
	private Optional<NGSIAttribute<NGSIAttribute<Date>>> dateNextCalculation;
	/**
	 * The date on which the KPI will be no longer necessary or meaningful.
	 * @AttributeType DateTime
	 * @Optional
	 */
	private Optional<NGSIAttribute<NGSIAttribute<Date>>> dateExpires;
	/**
	 * Last update date of the KPI data. This can be different than the last update date of the KPI's value.
	 * @AttributeType DateTime
	 * @Optional
	 */
	private Optional<NGSIAttribute<NGSIAttribute<Date>>> updateAt;
	/**
	 * Last update timestamp of this entity.
	 * @AttributeType DateTime
	 * @Optional
	 */
	private Optional<NGSIAttribute<NGSIAttribute<Date>>> dateModified;
	/**
	 * Location of the area to which the KPI refers to.
	 * @AttributeType geo:json
	 * @NormativeReferences https://tools.ietf.org/html/rfc7946
	 * @Mandatory If "address" is not defined
	 */
	private Optional<NGSIAttribute<? extends GeoJSON<Object>>> location;
	/**
	 * Civic address of the area to which the KPI refers to.
	 * @NormativeReferences https://schema.org/address
	 * @Mandatory If "location" is not defined
	 */
	private Optional<NGSIAttribute<String>> address;
	/**
	 * For organizational purposes, it allows to add extra textual geographical information such as district, burough, or any other hint which can help to identify the KPI coverage.
	 * @AttributeType Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> area;
	
	public KeyPerformanceIndicator() {
		super();
	}
	
	public KeyPerformanceIndicator(String id) {
		super(id);
	}
	
	
	public NGSIAttribute<String> getName() {
		return name;
	}
	public void setName(NGSIAttribute<String> name) {
		this.name = name;
	}
	public Optional<NGSIAttribute<String>> getAlternateName() {
		return alternateName;
	}
	public void setAlternateName(NGSIAttribute<String> alternateName) {
		this.alternateName = Optional.of(alternateName);
	}
	public NGSIAttribute<String> getOrganization() {
		return organization;
	}
	public void setOrganization(NGSIAttribute<String> organization) {
		this.organization = organization;
	}
	public NGSIAttribute<String> getProcess() {
		return process;
	}
	public void setProcess(NGSIAttribute<String> process) {
		this.process = process;
	}
	public NGSIAttribute<String> getProduct() {
		return product;
	}
	public void setProduct(NGSIAttribute<String> product) {
		this.product = product;
	}
	public Optional<NGSIAttribute<String>> getProvider() {
		return provider;
	}
	public void setProvider(NGSIAttribute<String> provider) {
		this.provider = Optional.of(provider);
	}
	public Optional<NGSIAttribute<String>> getBusinessTarget() {
		return businessTarget;
	}
	public void setBusinessTarget(NGSIAttribute<String> businessTarget) {
		this.businessTarget = Optional.of(businessTarget);
	}
	public Optional<NGSIAttribute<String>> getDescription() {
		return description;
	}
	public void setDescription(NGSIAttribute<String> description) {
		this.description = Optional.of(description);
	}
	public NGSIAttribute<String> getCalculationFrequency() {
		return calculationFrequency;
	}
	public void setCalculationFrequency(NGSIAttribute<String> calculationFrequency) {
		this.calculationFrequency = calculationFrequency;
	}
	public NGSIAttribute<List<String>> getCategory() {
		return category;
	}
	public void setCategory(NGSIAttribute<List<String>> category) {
		this.category = category;
	}
	public Optional<NGSIAttribute<String>> getCalculatedBy() {
		return calculatedBy;
	}
	public void setCalculatedBy(NGSIAttribute<String> calculatedBy) {
		this.calculatedBy = Optional.of(calculatedBy);
	}
	public Optional<NGSIAttribute<String>> getCalculationMethod() {
		return calculationMethod;
	}
	public void setCalculationMethod(NGSIAttribute<String> calculationMethod) {
		this.calculationMethod = Optional.of(calculationMethod);
	}
	public Optional<NGSIAttribute<String>> getCalculationFormula() {
		return calculationFormula;
	}
	public void setCalculationFormula(NGSIAttribute<String> calculationFormula) {
		this.calculationFormula = Optional.of(calculationFormula);
	}
	public Optional<NGSIAttribute<List<AggregatedData>>> getAggregatedData() {
		return aggregatedData;
	}
	public void setAggregatedData(NGSIAttribute<List<AggregatedData>> aggregatedData) {
		this.aggregatedData = Optional.of(aggregatedData);
	}
	public Optional<NGSIAttribute<CalculationPeriod>> getCalculationPeriod() {
		return calculationPeriod;
	}
	public void setCalculationPeriod(NGSIAttribute<CalculationPeriod> calculationPeriod) {
		this.calculationPeriod = Optional.of(calculationPeriod);
	}
	public Optional<NGSIAttribute<String>> getCurrentStanding() {
		return currentStanding;
	}
	public void setCurrentStanding(NGSIAttribute<String> currentStanding) {
		this.currentStanding = Optional.of(currentStanding);
	}
	public NGSIAttribute<String> getKpiValue() {
		return kpiValue;
	}
	public void setKpiValue(NGSIAttribute<String> kpiValue) {
		this.kpiValue = kpiValue;
	}
	public Optional<NGSIAttribute<NGSIAttribute<Date>>> getEffectiveSince() {
		return effectiveSince;
	}
	public void setEffectiveSince(NGSIAttribute<NGSIAttribute<Date>> effectiveSince) {
		this.effectiveSince = Optional.of(effectiveSince);
	}
	public Optional<NGSIAttribute<NGSIAttribute<Date>>> getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(NGSIAttribute<NGSIAttribute<Date>> dateCreated) {
		this.dateCreated = Optional.of(dateCreated);
	}
	public Optional<NGSIAttribute<NGSIAttribute<Date>>> getDateNextCalculation() {
		return dateNextCalculation;
	}
	public void setDateNextCalculation(NGSIAttribute<NGSIAttribute<Date>> dateNextCalculation) {
		this.dateNextCalculation = Optional.of(dateNextCalculation);
	}
	public Optional<NGSIAttribute<NGSIAttribute<Date>>> getDateExpires() {
		return dateExpires;
	}
	public void setDateExpires(NGSIAttribute<NGSIAttribute<Date>> dateExpires) {
		this.dateExpires = Optional.of(dateExpires);
	}
	public Optional<NGSIAttribute<NGSIAttribute<Date>>> getUpdateAt() {
		return updateAt;
	}
	public void setUpdateAt(NGSIAttribute<NGSIAttribute<Date>> updateAt) {
		this.updateAt = Optional.of(updateAt);
	}
	public Optional<NGSIAttribute<NGSIAttribute<Date>>> getDateModified() {
		return dateModified;
	}
	public void setDateModified(NGSIAttribute<NGSIAttribute<Date>> dateModified) {
		this.dateModified = Optional.of(dateModified);
	}
	public Optional<NGSIAttribute<? extends GeoJSON<Object>>> getLocation() {
		return location;
	}
	public void setLocation(NGSIAttribute<? extends GeoJSON<Object>> location) {
		this.location = Optional.of(location);
	}
	public Optional<NGSIAttribute<String>> getAddress() {
		return address;
	}
	public void setAddress(NGSIAttribute<String> address) {
		this.address = Optional.of(address);
	}
	public Optional<NGSIAttribute<String>> getArea() {
		return area;
	}
	public void setArea(NGSIAttribute<String> area) {
		this.area = Optional.of(area);
	}
}
