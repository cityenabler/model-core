package com.cityenabler.sdk.model;

public class NGSIMetadata<T> extends NGSIElement<T>{
	protected NGSIMetadata(){
		super();
	}
	
	public NGSIMetadata(T value){
		super(value);
	}
}
