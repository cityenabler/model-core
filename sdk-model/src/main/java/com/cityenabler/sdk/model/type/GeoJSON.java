package com.cityenabler.sdk.model.type;

import java.util.ArrayList;
import java.util.List;

import com.cityenabler.sdk.model.type.geojson.GeoJSONType;

public class GeoJSON<T> {
	private GeoJSONType type;
	private List<T> coordinates;
	
	public GeoJSON() {
		this.type = GeoJSONType.Point;
		this.coordinates = new ArrayList<T>();
	}
	
	public GeoJSON(GeoJSONType type) {
		this.type = type;
		this.coordinates = new ArrayList<T>();
	}
	
	public GeoJSON(GeoJSONType type, List<T> coordinates) {
		this.type = type;
		this.coordinates = coordinates;
	}
	
	public GeoJSONType getType() {
		return type;
	}
	
	public void setType(GeoJSONType type) {
		this.type = type;
	}
	
	public List<T> getCoordinates() {
		return coordinates;
	}
	
	public void setCoordinates(List<T> coordinates) {
		this.coordinates = coordinates;
	}
	
}
