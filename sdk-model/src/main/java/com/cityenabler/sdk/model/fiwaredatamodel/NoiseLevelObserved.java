package com.cityenabler.sdk.model.fiwaredatamodel;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.cityenabler.sdk.model.NGSIAttribute;
import com.cityenabler.sdk.model.NGSIEntity;
import com.cityenabler.sdk.model.type.GeoJSON;

public class NoiseLevelObserved extends NGSIEntity {
	/**
	 * Entity's creation timestamp.
	 * @AttributeType DateTime 
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateCreated;
	/** 
	 * Last update timestamp of this entity.
	 * @AttributeType DateTime
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateModified;	
	/**
	 * Location of this observation represented by a GeoJSON geometry.
	 * @AttributeType geo:json
	 * @NormativeReferences https://tools.ietf.org/html/rfc7946
	 * @Mandatory If "address" is not defined
	 */
	private Optional<NGSIAttribute<? extends GeoJSON<Object>>> location;
	/**
	 * Civic address of this observation.
	 * @NormativeReferences https://schema.org/address
	 * @Mandatory If "location" is not defined
	 */
	private NGSIAttribute<String> address; 
	/**
	 * Name given to this observation.
	 * @NormativeReferences https://schema.org/name
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> name;
	/**
	 * Description given to this observation.
	 * @NormativeReferences https://schema.org/description
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> description;
	
	/**
	 * The date and time of this observation in ISO8601 UTC format. It can be represented by an specific time instant or by an ISO8601 interval.
	 * As a workaround for the lack of support of Orion Context Broker for datetime intervals, it can be used two separate attributes: dateObservedFrom, dateObservedTo.
	 * @AttributeType DateTime
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateObserved;
	/**
	 * Observation period start date and time. See "dateObserved".
	 * @AttributeType DateTime
	 * @Optional
	 */
	private Optional<NGSIAttribute<Date>> dateObservedFrom;
	/**
	 * Observation period end date and time. See "dateObserved".
	 * @AttributeType DateTime
	 * @Optional
	 */
	private Optional<NGSIAttribute<Date>> dateObservedTo;
	/**
	 *  A reference to the device which captured this observation.
	 *  @AttributeType Reference to an entity of type "Device"
	 *  @Optional
	 */
	private Optional<NGSIAttribute<String>> refDevice;
	/**
	 * Class of sonometer (0, 1, 2) according to ANSI used for taking this observation. 
	 * This attribute is useful when no device entity is associated to observations. 
	 * It allows to convey, roughly, information about the precision of the measurements.
	 * 
	 * @AttributeType Text
	 * @AllowedValues  one of ("0", "1", "2")
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> sonometerClass;
	
	/**
	 * An array of strings containing details about each acoustic parameter observed.
	 * 
	 * @AttributeType List of Text
	 * @AllowedValues Each element of the array must be a string with the following format (a list of values separated by the | character): <measurand>| <observedValue>| <description>, where: 
	 * measurand : corresponds to a term defined at http://www.acoustic-glossary.co.uk/definitions-l.htm.
	 * observedValue : corresponds to the value for the measurand as a number expressed in decibels.
	 * description : short description of the measurand.
	 * Examples: "LAeq | 93.6 | A-weighted, equivalent, sound level"  "LAS | 91.6 | A-weighted, Slow, sound level"
	 * "LAeq,d | 65.4 | A-weighted, equivalent, day period, sound level"
	 * @Mandatory
	 */
	private NGSIAttribute<List<String>> measurand;
	
	public NoiseLevelObserved() {
		super();
	}
	
	public NoiseLevelObserved(String id) {
		super(id);
	}
	
	public NGSIAttribute<String> getAddress() {
		return address;
	}
	public void setAddress(NGSIAttribute<String> address) {
		this.address = address;
	}
	public Optional<NGSIAttribute<String>> getName() {
		return name;
	}
	public void setName(NGSIAttribute<String> name) {
		this.name = Optional.of(name);
	}
	public Optional<NGSIAttribute<String>> getDescription() {
		return description;
	}
	public void setDescription(NGSIAttribute<String> description) {
		this.description = Optional.of(description);
	}
	public Optional<NGSIAttribute<Date>> getDateObservedFrom() {
		return dateObservedFrom;
	}
	public void setDateObservedFrom(NGSIAttribute<Date> dateObservedFrom) {
		this.dateObservedFrom = Optional.of(dateObservedFrom);
	}
	public Optional<NGSIAttribute<Date>> getDateObservedTo() {
		return dateObservedTo;
	}
	public void setDateObservedTo(NGSIAttribute<Date> dateObservedTo) {
		this.dateObservedTo = Optional.of(dateObservedTo);
	}
	public Optional<NGSIAttribute<String>> getRefDevice() {
		return refDevice;
	}
	public void setRefDevice(NGSIAttribute<String> refDevice) {
		this.refDevice = Optional.of(refDevice);
	}
	public Optional<NGSIAttribute<String>> getSonometerClass() {
		return sonometerClass;
	}
	public void setSonometerClass(NGSIAttribute<String> sonometerClass) {
		this.sonometerClass = Optional.of(sonometerClass);
	}
	public NGSIAttribute<List<String>> getMeasurand() {
		return measurand;
	}
	public void setMeasurand(NGSIAttribute<List<String>> measurand) {
		this.measurand = measurand;
	}
	public NGSIAttribute<Date> getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(NGSIAttribute<Date> dateCreated) {
		this.dateCreated = dateCreated;
	}
	public NGSIAttribute<Date> getDateModified() {
		return dateModified;
	}
	public void setDateModified(NGSIAttribute<Date> dateModified) {
		this.dateModified = dateModified;
	}
	public Optional<NGSIAttribute<? extends GeoJSON<Object>>> getLocation() {
		return location;
	}
	public void setLocation(NGSIAttribute<? extends GeoJSON<Object>> location) {
		this.location = Optional.of(location);
	}
	public NGSIAttribute<Date> getDateObserved() {
		return dateObserved;
	}
	public void setDateObserved(NGSIAttribute<Date> dateObserved) {
		this.dateObserved = dateObserved;
	}
	
	
}
