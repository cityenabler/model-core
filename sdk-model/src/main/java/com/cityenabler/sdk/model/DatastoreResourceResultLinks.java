package com.cityenabler.sdk.model;

public class DatastoreResourceResultLinks {
	
	private String start;
	private String prev;
	private String next;
	
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getNext() {
		return next;
	}
	public void setNext(String next) {
		this.next = next;
	}
	public String getPrev() {
		return prev;
	}
	public void setPrev(String prev) {
		this.prev = prev;
	}
	
	
}
