package com.cityenabler.sdk.model.type;

import java.util.List;

public class AggregatedData {
	private String entityType;
	private List<String> attrs;
	
	public AggregatedData() {
	}
	public AggregatedData(String entityType, List<String> attrs) {
		this.entityType = entityType;
		this.attrs = attrs;
	}
	public String getEntityType() {
		return entityType;
	}
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	public List<String> getAttrs() {
		return attrs;
	}
	public void setAttrs(List<String> attrs) {
		this.attrs = attrs;
	}
	
	
}
