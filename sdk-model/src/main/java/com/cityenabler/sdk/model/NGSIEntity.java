package com.cityenabler.sdk.model;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Optional;

import com.cityenabler.sdk.model.type.builder.NGSIAttributeTypeBuilder;
import com.cityenabler.sdk.tool.SdkContext;
import com.cityenabler.sdk.tool.SdkUtils;

public class NGSIEntity extends NGSIObject{
	
	private String id;
	private String type;
	
	public <T> void setAttribute(String attrName, T attrValue){
		try{
			Field attrNameField = this.getClass().getDeclaredField(attrName);
				  attrNameField.setAccessible(true);
		  
			//The field is Optional
			if(attrNameField.getType().equals(Optional.class)){
				//Get internal value
				NGSIAttribute<T> ngsiAttr;
					String typeBuilderResult;
					typeBuilderResult = attrValue.getClass().getSimpleName();

					typeBuilderResult = SdkContext.getIstance().getTypeBuilder().mapType(attrValue);

					
					ngsiAttr = new NGSIAttribute<T>(typeBuilderResult,attrValue);
					attrNameField.set(this, Optional.of(ngsiAttr));
			}
			//The field is Mandatory
			else{
				ParameterizedType attrValueFieldType = (ParameterizedType) attrNameField.getGenericType();
				if(((Class<?>)attrValueFieldType.getActualTypeArguments()[0]).equals(attrValue.getClass())){
					NGSIAttribute<T> ngsiAttr;
					
					String typeBuilderResult;
					typeBuilderResult = attrValue.getClass().getSimpleName();
					try {
						typeBuilderResult = SdkContext.getIstance().getTypeBuilder().mapType(attrValue);
					} catch (IllegalArgumentException e) {
						throw new RuntimeException("Unknown type for attribute" + attrName);
					}
					
					ngsiAttr = new NGSIAttribute<T>(typeBuilderResult,attrValue);
					attrNameField.set(this, ngsiAttr); 
				}
				else{
					throw new RuntimeException("Invalid value "+attrValue+" for attribute " + attrName);
				}
			}
		}
		catch(NoSuchFieldException fe){
			throw new RuntimeException("No attribute "+attrName+" found");
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public NGSIEntity(String id, String type) {
		this.id = id;
		this.type = type;
	}
	
	public NGSIEntity(String id) {
		this.id = id;
		this.type = this.getClass().getSimpleName();
	}
	
	public NGSIEntity(String id, String type, NGSIAttributeTypeBuilder typeBuilder) {
		this.id = id;
		this.type = type;
		SdkContext.getIstance().setTypeBuilder(typeBuilder);
		
	}
	
	public NGSIEntity(String id, NGSIAttributeTypeBuilder typeBuilder) {
		this.id = id;
		this.type = this.getClass().getSimpleName();
		SdkContext.getIstance().setTypeBuilder(typeBuilder);
	}
	
	protected NGSIEntity() {
		this.id = this.getClass().getSimpleName() + "-" + SdkUtils.randomAlphanumeric(10);
		this.type = this.getClass().getSimpleName();
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NGSIEntity other = (NGSIEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	
}
