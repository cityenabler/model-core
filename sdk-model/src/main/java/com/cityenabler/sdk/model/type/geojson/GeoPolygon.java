package com.cityenabler.sdk.model.type.geojson;

import java.util.List;

import com.cityenabler.sdk.model.type.GeoJSON;

public class GeoPolygon extends GeoJSON<List<List<Double>>> {
	public GeoPolygon() {
		super(GeoJSONType.Polygon);
	}
	
	public GeoPolygon(List<List<List<Double>>> coordinates) {
		super(GeoJSONType.Polygon,coordinates);
	}
}
