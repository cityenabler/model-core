package com.cityenabler.sdk.model;

import java.util.ArrayList;
import java.util.List;

public class NGSISubscriptionCondition {
	private List<String> attrs;
	
	public NGSISubscriptionCondition() {
		this.attrs = new ArrayList<String>();
	}
	
	public NGSISubscriptionCondition(List<String> attrs) {
		this.attrs = attrs;
	}

	public List<String> getAttrs() {
		return attrs;
	}

	public void setAttrs(List<String> attrs) {
		this.attrs = attrs;
	}
	
	
}
