package com.cityenabler.sdk.model;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.function.Supplier;

public class NGSIDefaults {
	
	private static class DateTimeSupplier implements Supplier<Date>{

		public Date get() {
			return GregorianCalendar.getInstance().getTime();
		}
	}
	
	public static final String STRING_VALUE = "";
	public static final DateTimeSupplier DATETIME_VALUE = new DateTimeSupplier();
	public static final Float NUMBER_VALUE = 0.0F;
	public static final Object OBJECT_VALUE = new Object();
	
}
