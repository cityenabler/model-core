package com.cityenabler.sdk.model.fiwaredatamodel;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.cityenabler.sdk.model.NGSIAttribute;
import com.cityenabler.sdk.model.NGSIEntity;
import com.cityenabler.sdk.model.type.GeoJSON;

public class Museum extends NGSIEntity {
	
	/** 
	 * Last update timestamp of this entity.
	 * @AttributeType DateTime
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateModified;
	/**
	 * Entity's creation timestamp.
	 * @AttributeType DateTime 
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateCreated;
	/**
	 * A sequence of characters giving the source of the entity data.
	 * @AttributeType Text or URL
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> source;
	/**
	 * Name given to this museum.
	 * @NormativeReferences https://schema.org/name
	 * @Mandatory
	 */
	private NGSIAttribute<String> name;
	/**
	 * Alternative name given to this museum.
	 * @NormativeReferences https://schema.org/alternateName
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> alternateName;
	/**
	 * Description of this museum.
	 * @NormativeReferences https://schema.org/description
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> description;
	/**
	 *  Location of this museum represented by a GeoJSON geometry, usually a Point or a Polygon.
	 * @AttributeType geo:json
	 * @NormativeReferences https://tools.ietf.org/html/rfc7946
	 * @Mandatory If "address" is not defined
	 */
	private Optional<NGSIAttribute<? extends GeoJSON<Object>>> location;
	/**
	 * Address of this museum.
	 * @NormativeReferences https://schema.org/address
	 * @Mandatory If "location" is not defined
	 */
	private Optional<NGSIAttribute<String>> address;
	/**
	 * Type of museum according to the exhibited content.
	 * @AttributeType List of Text
	 * @AllowedValue (appliedArts, scienceAndTechnology, fineArts, music, history, sacredArt, archaeology, specials, decorativeArts,  literature, medicineAndPharmacy, maritime, transports, military, wax, popularArtsAndTraditions, numismatic, unesco, ceramics,  sumptuaryArts, naturalScience, prehistoric, ethnology, railway, mining, textile, sculpture, multiDisciplinar, painting,  paleonthology, modernArt, thematic, architecture, museumHouse, cathedralMuseum, diocesanMuseum, universitary, contemporaryArt,  bullfighting). Other possible source for museum types not covered above is Wikipedia.
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> museumType;
	/**
	 * Museum's owner.
	 * @AttributeType Text or a reference to an entity of type Person or Organization
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> owner;
	/**
	 * Main featured artist(s) at this museum.
	 * @AttributeType List of references to entities of type Person
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> featuredArtist;
	/**
	 * Corresponds to the historical period(s) of the exhibitions made by this museum.
	 * @AttributeType List of Text
	 * @AllowedValues A ISO8601 time interval. For example 1920/1940. The second element of the interval can be left empty to denote "till now".
	 * A comma separated list of years, for instance 1620,1625,1718.
	 * A century, represented by a year pattern, for instance 19xx would correspond to the twentieth century. And 196x would correspond to the sixties decade.
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> historicalPeriod;
	/**
	 * Corresponds to the art period(s) of the exhibitions made by this museum.
	 * @AttributeType List of Text
	 * @AllowedValues Those defined by Wikipedia.
	 * Any other extended value needed by an application and not described by the above resource.
	 * @Optional
	 */
	
	private Optional<NGSIAttribute<List<String>>> artPeriod;
	/**
	 * Type of building that hosts the museum.
	 * @AttributeType Text
	 * @AllowedValue Allowed values: (prehistoricPlace, acropolis, alcazaba, aqueduct, alcazar, amphitheatre, arch,  polularArchitecture, basilica, road,  chapel, cartuja, nobleHouse, residence, castle, castro, catacombs, cathedral,  cloister, convent, prehistoricCave, dolmen,  officeBuilding, houseBuilding, industrialBuilding, militaryBuilding,  hermitage, fortress, sculpturalGroups, church, garden, fishMarket,  masia, masiaFortificada, minaret, monastery,  monolith, walls, necropolis, menhir, mansion, palace, pantheon, pazo, pyramid,  bridge, gate, arcade, walledArea,  sanctuary, grave, synagogue, taulasTalayotsNavetas, theathre, temple, spring, tower,  archeologicalSite, university,  graveyard, fortifiedTemple, civilEngineering, square, seminar, bullfightingRing, publicBuilding, town,  cavesAndTouristicMines,  proCathedral, mosque, circus, burialMound)
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> buildingType;
	/**
	 *  Describes different facilities offered by this museum.
	 * @AttributeType List of Text
	 * @AllowedValues (elevator, cafeteria, shop, auditory, conferenceRoom,  audioguide, cloakRoom, forDisabled, forBabies, guidedTour,  restaurant, ramp, reservation) or any other value needed by an application.
	 * @Optional
	 */
	
	private Optional<NGSIAttribute<List<String>>> facilities;
	/**
	 * Opening hours specification for this museum.
	 * @NormativeReferences http://schema.org/openingHoursSpecification
	 * @AttributeType List of OpeningHoursSpecification
	 * @Optional
	 */	
	private Optional<NGSIAttribute<List<String>>> openingHoursSpecification;
	/**
	 * Contact point for the museum.
	 * @AttributeType https://schema.org/ContactPoint
	 * @Optional
	 * @ToDo 
	 */
	private Optional<NGSIAttribute<String>> contactPoint;
	/**
	 * Reference to one or more related entities.
	 * @AttributeType List of References
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> refSeeAlso;
	
	public Museum() {
		super();
	}
	
	public Museum(String id) {
		super(id);
	}
		
	public NGSIAttribute<Date> getDateModified() {
		return dateModified;
	}
	public void setDateModified(NGSIAttribute<Date> dateModified) {
		this.dateModified = dateModified;
	}
	public NGSIAttribute<Date> getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(NGSIAttribute<Date> dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Optional<NGSIAttribute<String>> getSource() {
		return source;
	}
	public void setSource(NGSIAttribute<String> source) {
		this.source = Optional.of(source);
	}
	public NGSIAttribute<String> getName() {
		return name;
	}
	public void setName(NGSIAttribute<String> name) {
		this.name = name;
	}
	public Optional<NGSIAttribute<String>> getAlternateName() {
		return alternateName;
	}
	public void setAlternateName(NGSIAttribute<String> alternateName) {
		this.alternateName = Optional.of(alternateName);
	}
	public Optional<NGSIAttribute<String>> getDescription() {
		return description;
	}
	public void setDescription(NGSIAttribute<String> description) {
		this.description = Optional.of(description);
	}
	public Optional<NGSIAttribute<? extends GeoJSON<Object>>> getLocation() {
		return location;
	}
	public void setLocation(NGSIAttribute<? extends GeoJSON<Object>> location) {
		this.location = Optional.of(location);
	}
	public Optional<NGSIAttribute<String>> getAddress() {
		return address;
	}
	public void setAddress(NGSIAttribute<String> address) {
		this.address = Optional.of(address);
	}
	public Optional<NGSIAttribute<List<String>>> getMuseumType() {
		return museumType;
	}
	public void setMuseumType(NGSIAttribute<List<String>> museumType) {
		this.museumType = Optional.of(museumType);
	}
	public Optional<NGSIAttribute<String>> getOwner() {
		return owner;
	}
	public void setOwner(NGSIAttribute<String> owner) {
		this.owner = Optional.of(owner);
	}
	public Optional<NGSIAttribute<List<String>>> getFeaturedArtist() {
		return featuredArtist;
	}
	public void setFeaturedArtist(NGSIAttribute<List<String>> featuredArtist) {
		this.featuredArtist = Optional.of(featuredArtist);
	}
	public Optional<NGSIAttribute<List<String>>> getHistoricalPeriod() {
		return historicalPeriod;
	}
	public void setHistoricalPeriod(NGSIAttribute<List<String>> historicalPeriod) {
		this.historicalPeriod = Optional.of(historicalPeriod);
	}
	public Optional<NGSIAttribute<List<String>>> getArtPeriod() {
		return artPeriod;
	}
	public void setArtPeriod(NGSIAttribute<List<String>> artPeriod) {
		this.artPeriod = Optional.of(artPeriod);
	}
	public Optional<NGSIAttribute<String>> getBuildingType() {
		return buildingType;
	}
	public void setBuildingType(NGSIAttribute<String> buildingType) {
		this.buildingType = Optional.of(buildingType);
	}
	public Optional<NGSIAttribute<List<String>>> getFacilities() {
		return facilities;
	}
	public void setFacilities(NGSIAttribute<List<String>> facilities) {
		this.facilities = Optional.of(facilities);
	}
	public Optional<NGSIAttribute<List<String>>> getOpeningHoursSpecification() {
		return openingHoursSpecification;
	}
	public void setOpeningHoursSpecification(NGSIAttribute<List<String>> openingHoursSpecification) {
		this.openingHoursSpecification = Optional.of(openingHoursSpecification);
	}
	public Optional<NGSIAttribute<String>> getContactPoint() {
		return contactPoint;
	}
	public void setContactPoint(NGSIAttribute<String> contactPoint) {
		this.contactPoint = Optional.of(contactPoint);
	}
	public Optional<NGSIAttribute<List<String>>> getRefSeeAlso() {
		return refSeeAlso;
	}
	public void setRefSeeAlso(NGSIAttribute<List<String>> refSeeAlso) {
		this.refSeeAlso = Optional.of(refSeeAlso);
	}
	

	
}
