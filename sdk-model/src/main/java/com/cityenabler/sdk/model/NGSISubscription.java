package com.cityenabler.sdk.model;

import java.util.Optional;

import com.cityenabler.sdk.tool.SdkUtils;

public class NGSISubscription extends NGSIObject{
	
	private String description;
	private Optional<String> expires;
	private String status;
	private NGSISubscriptionSubject subject;
	private NGSISubscriptionNotification notification;
	private Optional<Integer> throttling;
	
	public NGSISubscription() {
		this(new NGSISubscriptionSubject(),new NGSISubscriptionNotification());
	}
	
	public NGSISubscription(NGSISubscriptionSubject subject,NGSISubscriptionNotification notification) {
		this(NGSISubscription.class.getSimpleName() + "-" + SdkUtils.randomAlphanumeric(10),subject,notification);
	}
	
	public NGSISubscription(String id, NGSISubscriptionSubject subject,NGSISubscriptionNotification notification) {
		this(id,"Orion subscription",subject,notification);
	}
	
	public NGSISubscription(String id, String description, NGSISubscriptionSubject subject,NGSISubscriptionNotification notification) {
		this(id,description,"active",subject,notification);
	}
	
	public NGSISubscription(String id, String description, String status, NGSISubscriptionSubject subject,NGSISubscriptionNotification notification) {
		super(id);
		this.description = description;
		this.status = status;
		this.subject = subject;
		this.notification = notification;
	}

	public NGSISubscription(String id, String description, Optional<String> expires, String status,
			NGSISubscriptionSubject subject, NGSISubscriptionNotification notification, Integer throttling) {
		super(id);
		this.description = description;
		this.expires = expires;
		this.status = status;
		this.subject = subject;
		this.notification = notification;
		this.throttling = Optional.ofNullable(throttling);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Optional<String> getExpires() {
		return expires;
	}

	public void setExpires(Optional<String> expires) {
		this.expires = expires;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public NGSISubscriptionSubject getSubject() {
		return subject;
	}

	public void setSubject(NGSISubscriptionSubject subject) {
		this.subject = subject;
	}

	public NGSISubscriptionNotification getNotification() {
		return notification;
	}

	public void setNotification(NGSISubscriptionNotification notification) {
		this.notification = notification;
	}

	public Optional<Integer> getThrottling() {
		return throttling;
	}

	public void setThrottling(Integer throttling) {
		this.throttling = Optional.ofNullable(throttling);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NGSISubscription other = (NGSISubscription) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	
}
