package com.cityenabler.sdk.model;

public enum NGSIType  {
	Integer("Number"),
	Double("Number"),
	String("Text"),
	ArrayList("Array"),
	GeoJSON("geo:json"),
	GeoPoint("geo:json"),
	GeoPolygon("geo:json"),
	GeoMultiPolygon("geo:json");
	
	private String typeValue;
	
	private NGSIType(String typeValue) {
		this.typeValue = typeValue;
	}
	
	public String getTypeValue() {
		return typeValue;
	}
}
