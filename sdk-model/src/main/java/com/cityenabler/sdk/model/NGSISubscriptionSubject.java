package com.cityenabler.sdk.model;

import java.util.ArrayList;
import java.util.List;

public class NGSISubscriptionSubject {
	private List<NGSISubscriptionEntities> entities;
	private NGSISubscriptionCondition condition;
	
	public NGSISubscriptionSubject() {
		this.entities = new ArrayList<NGSISubscriptionEntities>();
		this.condition = new NGSISubscriptionCondition();
	}
	
	public NGSISubscriptionSubject(List<NGSISubscriptionEntities> entities) {
		this.entities = entities;
		this.condition = new NGSISubscriptionCondition();
	}
	
	public NGSISubscriptionSubject(NGSISubscriptionCondition condition) {
		this.entities = new ArrayList<NGSISubscriptionEntities>();
		this.condition = condition;
	}
	
	public NGSISubscriptionSubject(List<NGSISubscriptionEntities> entities, NGSISubscriptionCondition condition) {
		this.entities = entities;
		this.condition = condition;
	}
	
	public List<NGSISubscriptionEntities> getEntities() {
		return entities;
	}
	public void setEntities(List<NGSISubscriptionEntities> entities) {
		this.entities = entities;
	}
	public NGSISubscriptionCondition getCondition() {
		return condition;
	}
	public void setCondition(NGSISubscriptionCondition condition) {
		this.condition = condition;
	}
	
	
}
