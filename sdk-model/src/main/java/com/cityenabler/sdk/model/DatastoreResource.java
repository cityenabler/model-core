package com.cityenabler.sdk.model;

public class DatastoreResource {

	private String help;
	private Boolean success;
	private DatastoreResourceResult result;
	
	public DatastoreResourceResult getResult() {
		return result;
	}
	public void setResult(DatastoreResourceResult result) {
		this.result = result;
	}
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public String getHelp() {
		return help;
	}
	public void setHelp(String help) {
		this.help = help;
	}
	
}
