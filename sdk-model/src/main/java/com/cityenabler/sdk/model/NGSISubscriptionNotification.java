package com.cityenabler.sdk.model;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class NGSISubscriptionNotification {
	private Optional<Integer> timesSent;
	private Optional<String> lastNotification;
	private List<String> attrs;
	private Optional<String> attrsFormat;
	private Optional<NGSISubscriptionHTTPField> http;
	private Optional<NGSISubscriptionHTTPField> httpCustom;
	private Optional<String> lastSuccess;
	
	public NGSISubscriptionNotification() {
		this(Arrays.asList(),new NGSISubscriptionHTTPField());
	}
	
	public NGSISubscriptionNotification(List<String> attrs, NGSISubscriptionHTTPField http) {
		this.attrs = attrs;
		this.http = Optional.ofNullable(http);
	}
	
	public NGSISubscriptionNotification(Integer timesSent, String lastNotification,
			List<String> attrs, String attrsFormat, NGSISubscriptionHTTPField http,
			NGSISubscriptionHTTPField httpCustom, String lastSuccess) {
		
		if(Optional.ofNullable(timesSent).isPresent())
			this.timesSent = Optional.of(timesSent);
		
		if(Optional.ofNullable(lastNotification).isPresent())
			this.lastNotification = Optional.of(lastNotification);
		
		this.attrs = attrs;
		
		if(Optional.ofNullable(attrsFormat).isPresent())
			this.attrsFormat = Optional.of(attrsFormat);
		
		if(Optional.ofNullable(http).isPresent())
			this.http = Optional.of(http);
		
		if(Optional.ofNullable(httpCustom).isPresent())
			this.httpCustom = Optional.of(httpCustom);
		
		this.lastSuccess = Optional.of(lastSuccess);
	}
	
	public Optional<Integer> getTimesSent() {
		return timesSent;
	}
	
	public void setTimesSent(Integer timesSent) {
		this.timesSent = Optional.ofNullable(timesSent);
	}
	
	public Optional<String> getLastNotification() {
		return lastNotification;
	}
	
	public void setLastNotification(String lastNotification) {
		this.lastNotification = Optional.ofNullable(lastNotification);
	}
	
	public List<String> getAttrs() {
		return attrs;
	}
	
	public void setAttrs(List<String> attrs) {
		this.attrs = attrs;
	}
	
	public Optional<String> getAttrsFormat() {
		return attrsFormat;
	}
	
	public void setAttrsFormat(String attrsFormat) {
		this.attrsFormat = Optional.ofNullable(attrsFormat);
	}
	
	public Optional<NGSISubscriptionHTTPField> getHttp() {
		return http;
	}
	
	public void setHttp(NGSISubscriptionHTTPField http) {
		this.http = Optional.ofNullable(http);
	}
	
	public Optional<NGSISubscriptionHTTPField> getHttpCustom() {
		return httpCustom;
	}
	
	public void setHttpCustom(NGSISubscriptionHTTPField httpCustom) {
		this.httpCustom = Optional.ofNullable(httpCustom);
	}
	
	public Optional<String> getLastSuccess() {
		return lastSuccess;
	}
	
	public void setLastSuccess(String lastSuccess) {
		this.lastSuccess = Optional.ofNullable(lastSuccess);
	}
	
}
