package com.cityenabler.sdk.model.fiwaredatamodel;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.cityenabler.sdk.model.NGSIAttribute;
import com.cityenabler.sdk.model.NGSIEntity;
import com.cityenabler.sdk.model.type.GeoJSON;

public class OffStreetParking extends NGSIEntity {
	/** 
	 * Last update timestamp of this entity.
	 * @AttributeType DateTime
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateModified;
	/**
	 * Entity's creation timestamp.
	 * @AttributeType DateTime 
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateCreated;
	/**
	 * Geolocation of the parking site represented by a GeoJSON (Multi)Polygon or Point.
	 * @AttributeType geo:json
	 * @NormativeReferences https://tools.ietf.org/html/rfc7946
	 * @Mandatory If "address" is not defined
	 */
	private Optional<NGSIAttribute<? extends GeoJSON<Object>>> location;
	/**
	 * Registered parking site civic address.
	 * @NormativeReferences https://schema.org/address
	 * @Mandatory If "location" is not defined
	 */
	private Optional<NGSIAttribute<String>> address;
	/**
	 * Name given to the parking site.
	 * @NormativeReferences https://schema.org/name
	 * @Mandatory
	 */
	private NGSIAttribute<String> name;
	/**
	 * Parking site's category(ies). The purpose of this field is to allow to tag, generally speaking, off street parking entities. 
	 * Particularities and detailed descriptions should be found under the corresponding specific attributes.
	 * 
	 * @AttributeType List of Text
	 * @AllowedValues (public, private, publicPrivate, urbanDeterrentParking, parkingGarage, parkingLot, shortTerm, mediumTerm, longTerm,  free, feeCharged, staffed, guarded,  barrierAccess, gateAccess, freeAccess, forElectricalCharging,  onlyResidents, onlyWithPermit,  forEmployees, forVisitors,  forCustomers, forStudents, forMembers, forDisabled, forResidents,  underground, ground)
	 * The semantics of the forxxx values is that the parking offers specific spots subject to that particular condition.
	 * The semantics of the onlyxxxvalues is that the parking only allows to park on that particular condition.
	 * Other application-specific
	 * @Mandatory
	 */
	private NGSIAttribute<List<String>> category;
	/**
	 * Vehicle type(s) allowed. The first element of this array MUST be the principal vehicle type allowed. Free spot numbers of other allowed vehicle types might be reported under the attribute extraSpotNumber and through specific entities of type ParkingGroup.
	 * @AttributeType List of Text
	 * @AllowedValues The following values defined by VehicleTypeEnum, DATEX 2 version 2.3: (agriculturalVehicle, bicycle, bus, car, caravan,  carWithCaravan, carWithTrailer, constructionOrMaintenanceVehicle, lorry, moped, motorcycle, motorcycleWithSideCar,  motorscooter, tanker, trailer, van, anyVehicle)
	 * @Mandatory
	 */
	private NGSIAttribute<List<String>> allowedVehicleType;
	/**
	 * Type(s) of charge performed by the parking site.
	 * @AllowedValues Some of those defined by the DATEX II version 2.3 ChargeTypeEnum enumeration: (flat, minimum, maximum, additionalIntervalPrice seasonTicket temporaryPrice firstIntervalPrice, annualPayment, monthlyPayment,  free, other)
	 * Any other application-specific
	 * @Mandatory
	 */
	private NGSIAttribute<List<String>> chargeType;
	/**
	 * This attribute captures what permit(s) might be needed to park at this site. Semantics is that at least one of these permits is needed to park. When a permit is composed by more than one item (and) they can be combined with a ",". For instance "residentPermit,disabledPermit" stays that both, at the same time, a resident and a disabled permit are needed to park. If empty or null, no permit is needed.
	 * @AttributeType List of Text
	 * @AllowedValues Allowed values: The following, defined by the PermitTypeEnum enumeration of DATEX II version 2.3.
	 * oneOf (employeePermit, studentPermit, fairPermit, governmentPermit,  residentPermit, specificIdentifiedVehiclePermit, visitorPermit,  noPermitNeeded)
	 * Any other application-specific
	 * @Mandatory
	 */
	private NGSIAttribute<List<String>> requiredPermit;
	/**
	 * Occupancy detection method(s).
	 * @AttributeType List of Text
	 * @AllowedValues The following from DATEX II version 2.3 OccupancyDetectionTypeEnum: (none, balancing, singleSpaceDetection, modelBased, manual)
	 * Or any other application-specific
	 * @Mandatory
	 */
	private NGSIAttribute<List<String>> occupancyDetectionType;
	/**
	 * Accepted payment method(s).
	 * @NormativeReferences https://schema.org/acceptedPaymentMethod
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> acceptedPaymentMethod;
	/**
	 * Price rate per minute.
	 * @AttributeType Number
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> priceRatePerMinute;
	/**
	 * Price currency of price rate per minute.
	 * @NormativeReferences  https://schema.org/priceCurrency
	 * @AttributeType Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> priceCurrency;	
	/**
	 * Description about the parking site.
	 * @NormativeReferences https://schema.org/description
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> description;
	/**
	 * A URL containing a photo of this parking site.
	 * @NormativeReferences https://schema.org/image
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> image;
	/**
	 * Parking layout. Gives more details to the category attribute.
	 * @AttributeType Text
	 * @AllowedValues Allowed values: As per the ParkingLayoutEnum of DATEX II version 2.3: one Of (automatedParkingGarage, surface, multiStorey, singleLevel, multiLevel, openSpace, covered, nested, field, rooftop, sheds, carports, garageBoxes, other). 
	 * See also OpenStreetMap.Or any other value useful for the application and not covered above.
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> layout;
	/**
	 * Usage scenario(s). Gives more details to the category attribute.
	 * @AttributeType List of Text
	 * @AllowedValues Those defined by the enumeration ParkingUsageScenarioEnum of DATEX II version 2.3:(truckParking, parkAndRide, parkAndCycle,  parkAndWalk, kissAndRide, liftshare, carSharing,  restArea, serviceArea, dropOffWithValet,  dropOffMechanical, eventParking, automaticParkingGuidance,  staffGuidesToSpace,  vehicleLift, loadingBay, dropOff, overnightParking,  other)
	 * Or any other value useful for the application and not covered above.
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> usageScenario;
	/**
	 * Parking mode(s).
	 * @AttributeType List of Text
	 * @AllowedValues Those defined by the DATEX II version 2.3 ParkingModeEnum enumeration: (perpendicularParking, parallelParking, echelonParking)
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> parkingMode;
	/**
	 * Facilities provided by this parking site.
	 * @AttributeType List of Text
	 * @AllowedValues The following defined by the EquipmentTypeEnum enumeration of DATEX II version 2.3:(toilet, shower, informationPoint, internetWireless, payDesk, paymentMachine, cashMachine, vendingMachine, faxMachineOrService,  copyMachineOrService, safeDeposit, luggageLocker, publicPhone, elevator, dumpingStation freshWater, wasteDisposal, refuseBin,  iceFreeScaffold, playground, electricChargingStation, bikeParking, tollTerminal, defibrillator, firstAidEquipment fireHose fireExtinguisher fireHydrant)
	 * Any other application-specific
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> facilities;
	/**
	 * Security aspects provided by this parking site.
	 * @AttributeType List of Text
	 * @AllowedValues The following, some of them, defined by ParkingSecurityEnum of DATEX II version 2.3: (patrolled, securityStaff, externalSecurity, cctv, dog, guard24hours, lighting, floodLight, fences areaSeperatedFromSurroundings)
	 * Any other application-specific
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> security;
	/**
	 * For parking sites with multiple floor levels, highest floor.
	 * @AttributeType Number
	 * @AllowedValues An integer number. 0 is ground level. Upper floors are positive numbers. Lower floors are negative ones.
	 * @Optional
	 */
	private Optional<NGSIAttribute<Integer>> highestFloor;
	/**
	 * For parking sites with multiple floor levels, lowest floor.
	 * @AttributeType Number
	 * @AllowedValues An integer number.
	 * @Optional
	 */
	private Optional<NGSIAttribute<Integer>> lowestFloor;
	/**
	 * Maximum allowed stay at site, on a general basis, encoded as a ISO8601 duration. A null or empty value indicates an indefinite duration.
	 * @AttributeType Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> maximumParkingDuration;
	/**
	 * The total number of spots offered by this parking site. This number can be difficult to be obtained for those parking locations on which spots are not clearly marked by lines.
	 * @AttributeType Number
	 * @AllowedValues: Any positive integer number or 0.
	 * @NormativeReferences DATEX 2 version 2.3 attribute parkingNumberOfSpaces of the ParkingRecord class.
	 * @Optional
	 */
	private Optional<NGSIAttribute<Integer>> totalSpotNumber;
	/**
	 * The total number of spots offered by this parking site. This number can be difficult to be obtained for those parking locations on which spots are not clearly marked by lines.
	 * @AttributeType Number
	 * @AllowedValues A positive integer number, including 0. It must lower or equal than totalSpotNumber
	 * @Optional
	 */
	private Optional<NGSIAttribute<Integer>> availableSpotNumber;
	/**
	 * The number of extra spots available, i.e. free. This value must aggregate free spots from all groups mentioned below: A/ Those reserved for special purposes and usually require a permit. Permit details will be found at parking group level (entity of type ParkingGroup). B/ Those reserved for other vehicle types different than the principal allowed vehicle type. C/ Any other group of parking spots not subject to the general condition rules conveyed by this entity.
	 * @AttributeType Number
	 * @AllowedValues A positive integer number, including 0.
	 * @Optional
	 */
	private Optional<NGSIAttribute<Integer>> extraSpotNumber;
	/**
	 * Opening hours of the parking site.
	 * @NormativeReferences  http://schema.org/openingHours
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> openingHours;
	/**
	 * Number of the floor closest to the ground which currently has available parking spots.
	 * @AttributeType Number
	 * @AllowedValues Stories are numbered between -n and n, being 0 ground floor.
	 * @Optional
	 */
	private Optional<NGSIAttribute<Integer>> firstAvailableFloor;
	/**
	 * If the parking site is at a special location (airport, depatment store, etc.) it conveys what is such special location.
	 * @AttributeType Text
	 * @AllowedValues Those defined by ParkingSpecialLocationEnum of DATEX II version 2.3:
	 * (airportTerminal, exhibitonCentre, shoppingCentre, specificFacility, trainStation, campground, themePark, ferryTerminal,  vehicleOnRailTerminal, coachStation, cableCarStation, publicTransportStation, market, religiousCentre, conventionCentre, cinema,  skilift, hotel, other)
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> specialLocation;
	/**
	 * Status of the parking site.
	 * @AttributeType List of Text
	 * @AllowedValues The following defined by the following enumerations defined by DATEX II version 2.3 : ParkingSiteStatusEnum,OpeningStatusEnum,(open, closed, closedAbnormal,openingTimesInForce, full,  fullAtEntrance, spacesAvailable, almostFull)
	 * Or any other application-specific
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> status;
	/**
	 * Conditions for reservation.
	 * @AttributeType Text
	 * @AllowedValues The following specified by ReservationTypeEnum of DATEX II version 2.3: one Of (optional, mandatory, notAvailable, partly).
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> reservationType;
	/**
	 * Parking site's owner.
	 * @AttributeType Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> owner;
	/**
	 * Parking site service provider.
	 * @AttributeType Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> provider;
	/**
	 * The measures period related to availableSpotNumber and priceRatePerMinute.
	 * @AttributeType Number
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> measuresPeriod;
	/**
	 * The measures period related to availableSpotNumber and priceRatePerMinute.
	 * @AttributeType unitText
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> measuresPeriodUnit;
	/**
	 * Parking site contact point.
	 * @NormativeReferences https://schema.org/contactPoint
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> contactPoint;
	/**
	 * The average width of parking spots.
	 * @AttributeType Number
	 * @DefaultUnit Meters
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> avarageSpotWidth;
	/**
	 * The average length of parking spots.
	 * @AttributeType Number
	 * @DefaultUnit Meters
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> avarageSpotLength;
	/**
	 * Maximum allowed height for vehicles. If there are multiple zones, it will be the minimum height of all the zones.
	 * @AttributeType Number
	 * @DefaultUnit Meters
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> maximumAllowedHeight;
	/**
	 * Maximum allowed width for vehicles. If there are multiple zones, it will be the minimum width of all the zones.
	 * @AttributeType Number
	 * @DefaultUnit Meters
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> maximumAllowedWidth;
	/**
	 * Parking site's access point(s).
	 * @AttributeType List of references to ParkingAccess
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> refParkingAccess;
	/**
	 * Parking site's identified group(s). A group can correspond to a zone, a complete storey, a group of spots, etc.
	 * @AttributeType List of references to ParkingGroup
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> refParkingGroup ;
	/**
	 * Individual parking spots belonging to this offstreet parking site.
	 * @AttributeType List of references to ParkingSpot
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> refParkingSpot ;
	/**
	 * Area served by this parking site. Precise semantics can depend on the application or target city. For instance, it can be a neighbourhood, burough or district.
	 * @AttributeType Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> areaServed;
	/**
	 * Aggregated rating for this parking site.
	 * @NormativeReferences https://schema.org/aggregateRating
	 * @Optional
	 */
	private Optional<NGSIAttribute<Integer>> aggregateRating;
	
	public OffStreetParking() {
		super();
	}
	
	public OffStreetParking(String id) {
		super(id);
	}
	
	
	public Optional<NGSIAttribute<Integer>> getTotalSpotNumber() {
		return totalSpotNumber;
	}
	public void setTotalSpotNumber(NGSIAttribute<Integer> totalSpotNumber) {
		this.totalSpotNumber = Optional.of(totalSpotNumber);
	}
	public NGSIAttribute<Date> getDateModified() {
		return dateModified;
	}
	public void setDateModified(NGSIAttribute<Date> dateModified) {
		this.dateModified = dateModified;
	}
	public NGSIAttribute<Date> getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(NGSIAttribute<Date> dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Optional<NGSIAttribute<? extends GeoJSON<Object>>> getLocation() {
		return location;
	}
	public void setLocation(NGSIAttribute<? extends GeoJSON<Object>> location) {
		this.location = Optional.of(location);
	}
	public Optional<NGSIAttribute<String>> getAddress() {
		return address;
	}
	public void setAddress(NGSIAttribute<String> address) {
		this.address = Optional.of(address);
	}
	public NGSIAttribute<String> getName() {
		return name;
	}
	public void setName(NGSIAttribute<String> name) {
		this.name = name;
	}
	public NGSIAttribute<List<String>> getCategory() {
		return category;
	}
	public void setCategory(NGSIAttribute<List<String>> category) {
		this.category = category;
	}
	public NGSIAttribute<List<String>> getAllowedVehicleType() {
		return allowedVehicleType;
	}
	public void setAllowedVehicleType(NGSIAttribute<List<String>> allowedVehicleType) {
		this.allowedVehicleType = allowedVehicleType;
	}
	public NGSIAttribute<List<String>> getChargeType() {
		return chargeType;
	}
	public void setChargeType(NGSIAttribute<List<String>> chargeType) {
		this.chargeType = chargeType;
	}
	public NGSIAttribute<List<String>> getRequiredPermit() {
		return requiredPermit;
	}
	public void setRequiredPermit(NGSIAttribute<List<String>> requiredPermit) {
		this.requiredPermit = requiredPermit;
	}
	public NGSIAttribute<List<String>> getOccupancyDetectionType() {
		return occupancyDetectionType;
	}
	public void setOccupancyDetectionType(NGSIAttribute<List<String>> occupancyDetectionType) {
		this.occupancyDetectionType = occupancyDetectionType;
	}
	public Optional<NGSIAttribute<List<String>>> getAcceptedPaymentMethod() {
		return acceptedPaymentMethod;
	}
	public void setAcceptedPaymentMethod(NGSIAttribute<List<String>> acceptedPaymentMethod) {
		this.acceptedPaymentMethod = Optional.of(acceptedPaymentMethod);
	}
	public Optional<NGSIAttribute<Double>> getPriceRatePerMinute() {
		return priceRatePerMinute;
	}
	public void setPriceRatePerMinute(NGSIAttribute<Double> priceRatePerMinute) {
		this.priceRatePerMinute = Optional.of(priceRatePerMinute);
	}
	public Optional<NGSIAttribute<String>> getPriceCurrency() {
		return priceCurrency;
	}
	public void setPriceCurrency(NGSIAttribute<String> priceCurrency) {
		this.priceCurrency = Optional.of(priceCurrency);
	}
	public Optional<NGSIAttribute<String>> getDescription() {
		return description;
	}
	public void setDescription(NGSIAttribute<String> description) {
		this.description = Optional.of(description);
	}
	public Optional<NGSIAttribute<String>> getImage() {
		return image;
	}
	public void setImage(NGSIAttribute<String> image) {
		this.image = Optional.of(image);
	}
	public Optional<NGSIAttribute<String>> getLayout() {
		return layout;
	}
	public void setLayout(NGSIAttribute<String> layout) {
		this.layout = Optional.of(layout);
	}
	public Optional<NGSIAttribute<List<String>>> getUsageScenario() {
		return usageScenario;
	}
	public void setUsageScenario(NGSIAttribute<List<String>> usageScenario) {
		this.usageScenario = Optional.of(usageScenario);
	}
	public Optional<NGSIAttribute<List<String>>> getParkingMode() {
		return parkingMode;
	}
	public void setParkingMode(NGSIAttribute<List<String>> parkingMode) {
		this.parkingMode = Optional.of(parkingMode);
	}
	public Optional<NGSIAttribute<List<String>>> getFacilities() {
		return facilities;
	}
	public void setFacilities(NGSIAttribute<List<String>> facilities) {
		this.facilities = Optional.of(facilities);
	}
	public Optional<NGSIAttribute<List<String>>> getSecurity() {
		return security;
	}
	public void setSecurity(NGSIAttribute<List<String>> security) {
		this.security = Optional.of(security);
	}
	public Optional<NGSIAttribute<Integer>> getHighestFloor() {
		return highestFloor;
	}
	public void setHighestFloor(NGSIAttribute<Integer> highestFloor) {
		this.highestFloor = Optional.of(highestFloor);
	}
	public Optional<NGSIAttribute<Integer>> getLowestFloor() {
		return lowestFloor;
	}
	public void setLowestFloor(NGSIAttribute<Integer> lowestFloor) {
		this.lowestFloor = Optional.of(lowestFloor);
	}
	public Optional<NGSIAttribute<String>> getMaximumParkingDuration() {
		return maximumParkingDuration;
	}
	public void setMaximumParkingDuration(NGSIAttribute<String> maximumParkingDuration) {
		this.maximumParkingDuration = Optional.of(maximumParkingDuration);
	}
	public Optional<NGSIAttribute<Integer>> getAvailableSpotNumber() {
		return availableSpotNumber;
	}
	public void setAvailableSpotNumber(NGSIAttribute<Integer> availableSpotNumber) {
		this.availableSpotNumber = Optional.of(availableSpotNumber);
	}
	public Optional<NGSIAttribute<Integer>> getExtraSpotNumber() {
		return extraSpotNumber;
	}
	public void setExtraSpotNumber(NGSIAttribute<Integer> extraSpotNumber) {
		this.extraSpotNumber = Optional.of(extraSpotNumber);
	}
	public Optional<NGSIAttribute<String>> getOpeningHours() {
		return openingHours;
	}
	public void setOpeningHours(NGSIAttribute<String> openingHours) {
		this.openingHours = Optional.of(openingHours);
	}
	public Optional<NGSIAttribute<Integer>> getFirstAvailableFloor() {
		return firstAvailableFloor;
	}
	public void setFirstAvailableFloor(NGSIAttribute<Integer> firstAvailableFloor) {
		this.firstAvailableFloor = Optional.of(firstAvailableFloor);
	}
	public Optional<NGSIAttribute<String>> getSpecialLocation() {
		return specialLocation;
	}
	public void setSpecialLocation(NGSIAttribute<String> specialLocation) {
		this.specialLocation = Optional.of(specialLocation);
	}
	public Optional<NGSIAttribute<List<String>>> getStatus() {
		return status;
	}
	public void setStatus(NGSIAttribute<List<String>> status) {
		this.status = Optional.of(status);
	}
	public Optional<NGSIAttribute<String>> getReservationType() {
		return reservationType;
	}
	public void setReservationType(NGSIAttribute<String> reservationType) {
		this.reservationType = Optional.of(reservationType);
	}
	public Optional<NGSIAttribute<String>> getOwner() {
		return owner;
	}
	public void setOwner(NGSIAttribute<String> owner) {
		this.owner = Optional.of(owner);
	}
	public Optional<NGSIAttribute<String>> getProvider() {
		return provider;
	}
	public void setProvider(NGSIAttribute<String> provider) {
		this.provider = Optional.of(provider);
	}
	public Optional<NGSIAttribute<Double>> getMeasuresPeriod() {
		return measuresPeriod;
	}
	public void setMeasuresPeriod(NGSIAttribute<Double> measuresPeriod) {
		this.measuresPeriod = Optional.of(measuresPeriod);
	}
	public Optional<NGSIAttribute<String>> getMeasuresPeriodUnit() {
		return measuresPeriodUnit;
	}
	public void setMeasuresPeriodUnit(NGSIAttribute<String> measuresPeriodUnit) {
		this.measuresPeriodUnit = Optional.of(measuresPeriodUnit);
	}
	public Optional<NGSIAttribute<String>> getContactPoint() {
		return contactPoint;
	}
	public void setContactPoint(NGSIAttribute<String> contactPoint) {
		this.contactPoint = Optional.of(contactPoint);
	}
	public Optional<NGSIAttribute<Double>> getAvarageSpotWidth() {
		return avarageSpotWidth;
	}
	public void setAvarageSpotWidth(NGSIAttribute<Double> avarageSpotWidth) {
		this.avarageSpotWidth = Optional.of(avarageSpotWidth);
	}
	public Optional<NGSIAttribute<Double>> getAvarageSpotLength() {
		return avarageSpotLength;
	}
	public void setAvarageSpotLength(NGSIAttribute<Double> avarageSpotLength) {
		this.avarageSpotLength = Optional.of(avarageSpotLength);
	}
	public Optional<NGSIAttribute<Double>> getMaximumAllowedHeight() {
		return maximumAllowedHeight;
	}
	public void setMaximumAllowedHeight(NGSIAttribute<Double> maximumAllowedHeight) {
		this.maximumAllowedHeight = Optional.of(maximumAllowedHeight);
	}
	public Optional<NGSIAttribute<Double>> getMaximumAllowedWidth() {
		return maximumAllowedWidth;
	}
	public void setMaximumAllowedWidth(NGSIAttribute<Double> maximumAllowedWidth) {
		this.maximumAllowedWidth = Optional.of(maximumAllowedWidth);
	}
	public Optional<NGSIAttribute<List<String>>> getRefParkingAccess() {
		return refParkingAccess;
	}
	public void setRefParkingAccess(NGSIAttribute<List<String>> refParkingAccess) {
		this.refParkingAccess = Optional.of(refParkingAccess);
	}
	public Optional<NGSIAttribute<List<String>>> getRefParkingGroup() {
		return refParkingGroup;
	}
	public void setRefParkingGroup(NGSIAttribute<List<String>> refParkingGroup) {
		this.refParkingGroup = Optional.of(refParkingGroup);
	}
	public Optional<NGSIAttribute<List<String>>> getRefParkingSpot() {
		return refParkingSpot;
	}
	public void setRefParkingSpot(NGSIAttribute<List<String>> refParkingSpot) {
		this.refParkingSpot = Optional.of(refParkingSpot);
	}
	public Optional<NGSIAttribute<String>> getAreaServed() {
		return areaServed;
	}
	public void setAreaServed(NGSIAttribute<String> areaServed) {
		this.areaServed = Optional.of(areaServed);
	}
	public Optional<NGSIAttribute<Integer>> getAggregateRating() {
		return aggregateRating;
	}
	public void setAggregateRating(NGSIAttribute<Integer> aggregateRating) {
		this.aggregateRating = Optional.of(aggregateRating);
	}
	
	
}
