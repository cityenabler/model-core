package com.cityenabler.sdk.model;

public class NGSISubscriptionHTTPField {
	private String url;

	
	public NGSISubscriptionHTTPField() {
		this.url = "";
	}

	public NGSISubscriptionHTTPField(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}
