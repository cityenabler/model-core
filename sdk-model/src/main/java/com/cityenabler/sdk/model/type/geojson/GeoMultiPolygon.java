package com.cityenabler.sdk.model.type.geojson;

import java.util.List;

import com.cityenabler.sdk.model.type.GeoJSON;

public class GeoMultiPolygon extends GeoJSON<List<List<List<Double>>>> {
	public GeoMultiPolygon() {
		super(GeoJSONType.Multipolygon);
	}
	
	public GeoMultiPolygon(List<List<List<List<Double>>>> coordinates) {
		super(GeoJSONType.Polygon,coordinates);
	}
}
