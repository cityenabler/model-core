package com.cityenabler.sdk.model;

import java.util.Optional;

public class NGSISubscriptionEntities {
	private Optional<String> id;
	private Optional<String> idPattern;
	private Optional<String> type;
	
	public NGSISubscriptionEntities() {
		this.idPattern = Optional.ofNullable(".*");
	}
	
	public NGSISubscriptionEntities(String id, Boolean isIdPattern) {
		if(isIdPattern)	this.idPattern = Optional.ofNullable(id);
		else this.id = Optional.ofNullable(id);
	}
	
	public NGSISubscriptionEntities(String id, String idPattern) {
		this.id = Optional.ofNullable(id);
		this.idPattern = Optional.ofNullable(idPattern);
	}
	
	public NGSISubscriptionEntities(String id, String idPattern, String type) {
		this.id = Optional.ofNullable(id);
		this.idPattern = Optional.ofNullable(idPattern);
		this.type = Optional.ofNullable(type);
	}
	
	public Optional<String> getIdPattern() {
		return idPattern;
	}
	
	public void setIdPattern(String idPattern) {
		this.idPattern = Optional.ofNullable(idPattern);
	}
	
	public Optional<String> getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = Optional.ofNullable(id);
	}
	
	public Optional<String> getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = Optional.ofNullable(type);
	}
	
	
}
