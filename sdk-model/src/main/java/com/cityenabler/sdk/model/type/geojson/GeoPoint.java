package com.cityenabler.sdk.model.type.geojson;

import java.util.Arrays;
import com.cityenabler.sdk.model.type.GeoJSON;

public class GeoPoint extends GeoJSON<Double> {
	public GeoPoint() {
		super(GeoJSONType.Point);
	}
	
	public GeoPoint(Double lon,Double lat) {
		super(GeoJSONType.Point,Arrays.asList(lon,lat));
	}
}
