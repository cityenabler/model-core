package com.cityenabler.sdk.model.fiwaredatamodel;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.cityenabler.sdk.model.NGSIAttribute;
import com.cityenabler.sdk.model.NGSIEntity;
import com.cityenabler.sdk.model.type.GeoJSON;

public class GreenspaceRecord extends NGSIEntity {
	
	/** 
	 * Last update timestamp of this entity.
	 * @AttributeType DateTime
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateModified;
	/**
	 * Entity's creation timestamp.
	 * @AttributeType DateTime 
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateCreated;
	/**
	 * A sequence of characters giving the source of the entity data.
	 * @AttributeType Text or URL
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> source;
	/**
	 * Location of the area concerned by this record and represented by a GeoJSON geometry.
	 * @AttributeType geo:json
	 * @NormativeReferences https://tools.ietf.org/html/rfc7946
	 * @Mandatory 
	 */
	private NGSIAttribute<? extends GeoJSON<Object>> location;
	/**
	 * The date and time of this observation in ISO8601 UTCformat.
	 * @AttributeType DateTime 
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateObserved;
	/**
	 * The observed soil temperature in Celsius degrees.
	 * @AttributeType Number
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> soilTemperature;
	/**
	 * The observed soil moisture measured as Volumetric Water Content, VWC (percentage, expressed in parts per one).
	 * @AttributeType Number between 0 and 1
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> soilMoistureVwc;
	/**
	 * The observed soild moisture measured as Electrical Conductivity, EC in units of Siemens per meter (S/m).
	 * @AttributeType Number
	 * @DefaultUnit Siemens per meter (S/m)
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> soilMoistureEc;
	/**
	 * The garden or flower bed to which this record refers to.
	 * @AttributeType Reference to an entity of type Garden or FlowerBed
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> refGreenspace;
	/**
	 * The device or devices used to obtain the data expressed by this record.
	 * @AttributeType Reference to an entity of type Device
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> refDevice;
	/**
	 * The weather conditions (air temperature, humidity, etc.) observed at the area.
	 * @AttributeType Reference to an entity of type WeatherObserved
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> refWeatherObserved;
	
	public GreenspaceRecord() {
		super();
	}
	
	public GreenspaceRecord(String id) {
		super(id);
	}
	
	
	public NGSIAttribute<Date> getDateModified() {
		return dateModified;
	}
	public void setDateModified(NGSIAttribute<Date> dateModified) {
		this.dateModified = dateModified;
	}
	public NGSIAttribute<Date> getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(NGSIAttribute<Date> dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Optional<NGSIAttribute<String>> getSource() {
		return source;
	}
	public void setSource(NGSIAttribute<String> source) {
		this.source = Optional.of(source);
	}
	public NGSIAttribute<? extends GeoJSON<Object>> getLocation() {
		return location;
	}
	public void setLocation(NGSIAttribute<? extends GeoJSON<Object>> location) {
		this.location = location;
	}
	public NGSIAttribute<Date> getDateObserved() {
		return dateObserved;
	}
	public void setDateObserved(NGSIAttribute<Date> dateObserved) {
		this.dateObserved = dateObserved;
	}
	public Optional<NGSIAttribute<Double>> getSoilTemperature() {
		return soilTemperature;
	}
	public void setSoilTemperature(NGSIAttribute<Double> soilTemperature) {
		this.soilTemperature = Optional.of(soilTemperature);
	}
	public Optional<NGSIAttribute<Double>> getSoilMoistureVwc() {
		return soilMoistureVwc;
	}
	public void setSoilMoistureVwc(NGSIAttribute<Double> soilMoistureVwc) {
		this.soilMoistureVwc = Optional.of(soilMoistureVwc);
	}
	public Optional<NGSIAttribute<Double>> getSoilMoistureEc() {
		return soilMoistureEc;
	}
	public void setSoilMoistureEc(NGSIAttribute<Double> soilMoistureEc) {
		this.soilMoistureEc = Optional.of(soilMoistureEc);
	}
	public Optional<NGSIAttribute<List<String>>> getRefGreenspace() {
		return refGreenspace;
	}
	public void setRefGreenspace(NGSIAttribute<List<String>> refGreenspace) {
		this.refGreenspace = Optional.of(refGreenspace);
	}
	public Optional<NGSIAttribute<List<String>>> getRefDevice() {
		return refDevice;
	}
	public void setRefDevice(NGSIAttribute<List<String>> refDevice) {
		this.refDevice = Optional.of(refDevice);
	}
	public Optional<NGSIAttribute<List<String>>> getRefWeatherObserved() {
		return refWeatherObserved;
	}
	public void setRefWeatherObserved(NGSIAttribute<List<String>> refWeatherObserved) {
		this.refWeatherObserved = Optional.of(refWeatherObserved);
	}
	
	
	
	
}
