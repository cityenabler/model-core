package com.cityenabler.sdk.model.type.builder;

public interface NGSIAttributeTypeBuilder {
		/**
		 * @param entity of type T
		 * @return the entity T serialized in json string
		 */
		public String mapType(Object T);
}
