package com.cityenabler.sdk.model;

public abstract class NGSIElement<T> {
	private String type;
	private T value;
	
	protected NGSIElement(){}
	
	public NGSIElement(T value){
		this.setValue(value);
		Class<?> valueClazz = this.value.getClass();
		
		this.setType(valueClazz.getSimpleName());
	}
	
	public NGSIElement(String type,T value) {
		this.setType(type);
		this.setValue(value);
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	public T getValue() {
		return value;
	}
	public void setValue(T value) {
		this.value = value;
	}

}
