package com.cityenabler.sdk.model.fiwaredatamodel;

import java.util.Date;
import java.util.Optional;

import com.cityenabler.sdk.model.NGSIAttribute;
import com.cityenabler.sdk.model.NGSIEntity;
import com.cityenabler.sdk.model.type.GeoJSON;

public class AirQualityObserved extends NGSIEntity {
	/** 
	 * Last update timestamp of this entity.
	 * @AttributeType DateTime
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateModified;
	/**
	 * Entity's creation timestamp.
	 * @AttributeType DateTime 
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateCreated;
	/**
	 * Location of the air quality observation represented by a GeoJSON geometry.
	 * @AttributeType geo:json
	 * @NormativeReferences https://tools.ietf.org/html/rfc7946
	 * @Mandatory If "address" is not defined
	 */
	private Optional<NGSIAttribute<? extends GeoJSON<Object>>> location;
	/**
	 * Civic address of the air quality observation. Sometimes it corresponds to the air quality station address.
	 * @NormativeReferences https://schema.org/address
	 * @Mandatory If "location" is not defined
	 * @Todo Address Bean
	 */
	private NGSIAttribute<String> address; 
	/**
	 * The date and time of this observation in ISO8601 UTCformat. It can be represented by an specific time instant or by an ISO8601 interval.
	 * @AttributeType DateTime
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateObserved;
	/**
	 * A sequence of characters giving the source of the entity data.
	 * @AttributeType Text or URL
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> source;
	/**
	 * Overall qualitative level of health concern corresponding to the air quality observed.
	 * @AttributeType Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> airQualityLevel; 
	/**
	 * Air quality index corresponding to the air quality observed.
	 * @AttributeType Number
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> airQualityIndex;
	/**
	 * Reliability (percentage, expressed in parts per one) corresponding to the air quality observed.
	 * @AttributeType Number
	 * @AllowedValues Interval[0,1]
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> reliability;
	/**
	 *  A reference to the device(s) which captured this observation.
	 *  @AttributeType Reference to an entity of type "Device"
	 *  @Optional
	 */
	private Optional<NGSIAttribute<String>> refDevice;
	/**
	 *  A reference to a point of interest (usually an air quality station) associated to this observation.
	 *  @AttributeType Reference to an entity of type "PointOfInterest"
	 *  @Optional
	 */
	private Optional<NGSIAttribute<String>> refPointOfInterest;
	
	public AirQualityObserved() {
		super();
	}
	
	public AirQualityObserved(String id) {
		super(id);
	}
	
	public Optional<NGSIAttribute<String>> getSource() {
		return source;
	}
	public void setSource(NGSIAttribute<String> source) {
		this.source = Optional.of(source);
	}
	public Optional<NGSIAttribute<Double>> getReliability() {
		return reliability;
	}
	public void setReliability(NGSIAttribute<Double> reliability) {
		this.reliability = Optional.of(reliability);
	}
	public Optional<NGSIAttribute<String>> getRefDevice() {
		return refDevice;
	}
	public void setRefDevice(NGSIAttribute<String> refDevice) {
		this.refDevice = Optional.of(refDevice);
	}
	public Optional<NGSIAttribute<String>> getRefPointOfInterest() {
		return refPointOfInterest;
	}
	public void setRefPointOfInterest(NGSIAttribute<String> refPointOfInterest) {
		this.refPointOfInterest = Optional.of(refPointOfInterest);
	}
	public NGSIAttribute<Date> getDateModified() {
		return dateModified;
	}
	public void setDateModified(NGSIAttribute<Date> dateModified) {
		this.dateModified = dateModified;
	}
	public NGSIAttribute<Date> getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(NGSIAttribute<Date> dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Optional<NGSIAttribute<? extends GeoJSON<Object>>> getLocation() {
		return location;
	}
	public void setLocation(NGSIAttribute<? extends GeoJSON<Object>> location) {
		this.location = Optional.of(location);
	}
	public NGSIAttribute<String> getAddress() {
		return address;
	}
	public void setAddress(NGSIAttribute<String> address) {
		this.address = address;
	}
	public NGSIAttribute<Date> getDateObserved() {
		return dateObserved;
	}
	public void setDateObserved(NGSIAttribute<Date> dateObserved) {
		this.dateObserved = dateObserved;
	}
	public Optional<NGSIAttribute<String>> getAirQualityLevel() {
		return airQualityLevel;
	}
	public void setAirQualityLevel(NGSIAttribute<String> airQualityLevel) {
		this.airQualityLevel = Optional.of(airQualityLevel);
	}
	public Optional<NGSIAttribute<Double>> getAirQualityIndex() {
		return airQualityIndex;
	}
	public void setAirQualityIndex(NGSIAttribute<Double> airQualityIndex) {
		this.airQualityIndex = Optional.of(airQualityIndex);
	}
	
	
	
	
}
