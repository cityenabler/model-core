package com.cityenabler.sdk.model.type.builder;

public  class TypeBuilder implements NGSIAttributeTypeBuilder {
   
   public TypeBuilder() {}

   @Override
   public String mapType(Object T) {
	  return T.getClass().getSimpleName();
   }

}
