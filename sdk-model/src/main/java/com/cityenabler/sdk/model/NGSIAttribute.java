package com.cityenabler.sdk.model;

import java.util.HashMap;
import java.util.Map;

public class NGSIAttribute<T> extends NGSIElement<T>{

	private Map<String, NGSIMetadata<?>> metadata = new HashMap<String, NGSIMetadata<?>>();
	
	public NGSIAttribute(){
		super();
	}
	
	public NGSIAttribute(T value){
		super(value);
	}
	
	public NGSIAttribute(String mapType, T attrValue) {
		super(mapType,attrValue);
	}

	public Map<String, NGSIMetadata<?>> getMetadata() {
		return metadata;
	}
	public void setMetadata(Map<String, NGSIMetadata<?>> metadata) {
		this.metadata = metadata;
	}
	
	public <V> void addMetadata(String key, V value){
		this.getMetadata().put(key, new NGSIMetadata<V>(value));
	}
	
	public void removeMetadata(String key){
		this.getMetadata().remove(key);
	}
	
	public void getMetadata(String key){
		this.getMetadata().get(key);
	}

}
