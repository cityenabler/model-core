package com.cityenabler.sdk.model.fiwaredatamodel;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.cityenabler.sdk.model.NGSIAttribute;
import com.cityenabler.sdk.model.NGSIEntity;
import com.cityenabler.sdk.model.type.GeoJSON;

public class BikeHireDockingStation extends NGSIEntity {
	/** 
	 * Last update timestamp of this entity.
	 * @AttributeType DateTime
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateModified;
	/**
	 * Entity's creation timestamp.
	 * @AttributeType DateTime 
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateCreated;
	/**
	 * Geolocation of the station represented by a GeoJSON (Multi)Polygon or Point.
	 * @AttributeType geo:json
	 * @NormativeReferences https://tools.ietf.org/html/rfc7946
	 * @Mandatory If "address" is not defined
	 */
	private Optional<NGSIAttribute<? extends GeoJSON<Object>>> location;
	/**
	 * Registered docking station site civic address.
	 * @NormativeReferences https://schema.org/address
	 * @Mandatory If "location" is not defined
	 */
	private Optional<NGSIAttribute<String>> address;
	/**
	 * Name given to the docking station.
	 * @NormativeReferences https://schema.org/name
	 * @Mandatory
	 */
	private NGSIAttribute<String> name;
	/**
	 * Name given to the docking station.
	 * @NormativeReferences https://schema.org/description
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> description;
	/**
	 * A URL containing a photo of this docking station
	 * @NormativeReferences https://schema.org/image
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> image;
	/**
	 * The total number of slots offered by this bike docking station.
	 * @AttributeType Number
	 * @AllowedValuesA Any positive integer number or 0.
	 * @Optional
	 */
	private Optional<NGSIAttribute<Integer>> totalSlotNumber;
	/**
	 * The number of slots available for returning and parking bikes. It must lower or equal than totalSlotNumber.
	 * @AttributeType Number
	 * @AllowedValuesA positive integer number, including 0. It must lower or equal than totalSlotNumber
	 * @Optional
	 */
	private Optional<NGSIAttribute<Integer>> freeSlotNumber;
	/**
	 * The number of slots that are out of order and cannot be used to hire or park a bike. It must lower or equal than totalSlotNumber
	 * @AttributeType Number
	 * @AllowedValuesA A positive integer number, including 0.
	 * @Optional
	 */
	private Optional<NGSIAttribute<Integer>> outOfServiceSlotNumber ;
	/**
	 * The number of bikes available in the bike hire docking station to be hired by users.
	 * @AttributeType Number
	 * @AllowedValuesA A positive integer number, including 0.
	 * @Optional
	 */
	private Optional<NGSIAttribute<Integer>> availableBikeNumber ;
	/**
	 * Opening hours of the docking station.
	 * @NormativeReferences  http://schema.org/openingHours
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> openingHours;
	/**
	 * Status of the bike hire docking station.
	 * @AttributeType List of Text
	 * @AllowedValues (working, outOfService, withIncidence, full, almostFull, empty, almostEmpty) Or any other application+specific.
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> status;
	/**
	 * Area served by this docking station. Precise semantics can depend on the application or target city. For instance, it can be a neighbourhood, burough or district.
	 * @AttributeType Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> areaServed;
	/**
	 * Bike hire docking station's owner.
	 * @AttributeType Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> owner;
	/**
	 * Bike hire service provider.
	 * @NormativeReferences https://schema.org/provider
	 * @AttributeType Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> provider;
	/**
	 * Bike hire service contact point.
	 * @NormativeReferences https://schema.org/contactPoint
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> contactPoint;
	
	public BikeHireDockingStation() {
		super();
	}
	
	public BikeHireDockingStation(String id) {
		super(id);
	}
	
	
	public NGSIAttribute<Date> getDateModified() {
		return dateModified;
	}
	public void setDateModified(NGSIAttribute<Date> dateModified) {
		this.dateModified = dateModified;
	}
	public NGSIAttribute<Date> getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(NGSIAttribute<Date> dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Optional<NGSIAttribute<? extends GeoJSON<Object>>> getLocation() {
		return location;
	}
	public void setLocation(NGSIAttribute<? extends GeoJSON<Object>> location) {
		this.location = Optional.of(location);
	}
	public Optional<NGSIAttribute<String>> getAddress() {
		return address;
	}
	public void setAddress(NGSIAttribute<String> address) {
		this.address = Optional.of(address);
	}
	public NGSIAttribute<String> getName() {
		return name;
	}
	public void setName(NGSIAttribute<String> name) {
		this.name = name;
	}
	public Optional<NGSIAttribute<String>> getDescription() {
		return description;
	}
	public void setDescription(NGSIAttribute<String> description) {
		this.description = Optional.of(description);
	}
	public Optional<NGSIAttribute<String>> getImage() {
		return image;
	}
	public void setImage(NGSIAttribute<String> image) {
		this.image = Optional.of(image);
	}
	public Optional<NGSIAttribute<Integer>> getTotalSlotNumber() {
		return totalSlotNumber;
	}
	public void setTotalSlotNumber(NGSIAttribute<Integer> totalSlotNumber) {
		this.totalSlotNumber = Optional.of(totalSlotNumber);
	}
	public Optional<NGSIAttribute<Integer>> getFreeSlotNumber() {
		return freeSlotNumber;
	}
	public void setFreeSlotNumber(NGSIAttribute<Integer> freeSlotNumber) {
		this.freeSlotNumber = Optional.of(freeSlotNumber);
	}
	public Optional<NGSIAttribute<Integer>> getOutOfServiceSlotNumber() {
		return outOfServiceSlotNumber;
	}
	public void setOutOfServiceSlotNumber(NGSIAttribute<Integer> outOfServiceSlotNumber) {
		this.outOfServiceSlotNumber = Optional.of(outOfServiceSlotNumber);
	}
	public Optional<NGSIAttribute<Integer>> getAvailableBikeNumber() {
		return availableBikeNumber;
	}
	public void setAvailableBikeNumber(NGSIAttribute<Integer> availableBikeNumber) {
		this.availableBikeNumber = Optional.of(availableBikeNumber);
	}
	public Optional<NGSIAttribute<String>> getOpeningHours() {
		return openingHours;
	}
	public void setOpeningHours(NGSIAttribute<String> openingHours) {
		this.openingHours = Optional.of(openingHours);
	}
	public Optional<NGSIAttribute<List<String>>> getStatus() {
		return status;
	}
	public void setStatus(NGSIAttribute<List<String>> status) {
		this.status = Optional.of(status);
	}
	public Optional<NGSIAttribute<String>> getAreaServed() {
		return areaServed;
	}
	public void setAreaServed(NGSIAttribute<String> areaServed) {
		this.areaServed = Optional.of(areaServed);
	}
	public Optional<NGSIAttribute<String>> getOwner() {
		return owner;
	}
	public void setOwner(NGSIAttribute<String> owner) {
		this.owner = Optional.of(owner);
	}
	public Optional<NGSIAttribute<String>> getProvider() {
		return provider;
	}
	public void setProvider(NGSIAttribute<String> provider) {
		this.provider = Optional.of(provider);
	}
	public Optional<NGSIAttribute<String>> getContactPoint() {
		return contactPoint;
	}
	public void setContactPoint(NGSIAttribute<String> contactPoint) {
		this.contactPoint = Optional.of(contactPoint);
	}
	
}
