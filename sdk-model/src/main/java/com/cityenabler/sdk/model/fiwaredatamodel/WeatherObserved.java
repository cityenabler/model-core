package com.cityenabler.sdk.model.fiwaredatamodel;

import java.util.Date;
import java.util.Optional;

import com.cityenabler.sdk.model.NGSIAttribute;
import com.cityenabler.sdk.model.NGSIEntity;
import com.cityenabler.sdk.model.type.GeoJSON;

public class WeatherObserved extends NGSIEntity {
	/**
	 * Entity's creation timestamp.
	 * @AttributeType DateTime 
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateCreated;
	/** 
	 * Last update timestamp of this entity.
	 * @AttributeType DateTime
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateModified;	
	/**
	 * Location of the weather observation represented by a GeoJSON geometry.
	 * @AttributeType geo:json
	 * @NormativeReferences https://tools.ietf.org/html/rfc7946
	 * @Mandatory If "address" is not defined
	 */
	private Optional<NGSIAttribute<? extends GeoJSON<Object>>> location;
	/**
	 * Civic address of the weather observation. Sometimes it corresponds to a weather station address
	 * @NormativeReferences https://schema.org/address
	 * @Mandatory If "location" is not defined
	 */
	private NGSIAttribute<String> address; 
	/**
	 * Name given to the weather observed location
	 * @NormativeReferences https://schema.org/name
	 * @Optional
	 */
	private NGSIAttribute<String> name;	
	/**
	 * The date and time of this observation in ISO8601 UTCformat. It can be represented by an specific time instant or by an ISO8601 interval
	 * @AttributeType DateTime
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateObserved;
	/**
	 * A sequence of characters giving the source of the entity data.
	 * @AttributeType Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> source;

	/**
	 *  A reference to the device(s) which captured this observation
	 *  @AttributeType Reference to an entity of type Device
	 *  @Optional
	 */
	private Optional<NGSIAttribute<String>> refDevice;

	/**
	 *  A reference to a point of interest (usually a weather station) associated to this observation.
	 *  @AttributeType Reference to an entity of type PointOfInterest
	 *  @Optional
	 */
	private Optional<NGSIAttribute<String>> refPointOfInterest;	
	/**
	 * The observed weather type. It is represented by a comma separated list of weather statuses, for instance  overcast, lightRain. A proposed mapping for Spanish terms can be found here.
	 * @AttributeType Text
	 * @AllowedValues A combination of (clearNight,sunnyDay, slightlyCloudy, partlyCloudy, mist, fog, highClouds, cloudy, veryCloudy,  overcast,  lightRainShower, drizzle, lightRain, heavyRainShower, heavyRain, sleetShower, sleet,  hailShower, hail, shower, lightSnow, snow, heavySnowShower,  heavySnow, thunderShower, thunder) or any other extended value.
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> weatherType;
	/**
	 * The dew point encoded as a number.
	 * @AttributeType Number
	 * @DefaultUnit Celsius Degrees
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> dewPoint;
	/**
	 * Visibility reported.
	 * @AttributeType Text
	 * @AllowedValues  One of (veryPoor, poor, moderate, good, veryGood, excellent)
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> visibility;
	/**
	 * Air's temperature observed.
	 * @AttributeType Number
	 * @DefaultUnit Degrees Centigrades
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> temperature;
	/**
	 *  Air's relative humidity observed (percentage, expressed in parts per one).
	 * @AttributeType Number
	 * @AllowedValues A number between 0 and 1
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> relativeHumidity;
	/**
	 * Precipitation level observed.
	 * @AttributeType Number
	 * @DefaultUnit Liters per square meter
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> precipitation;
	/**
	 * The wind direction expressed in decimal degrees compared to geographic North (measured clockwise), encoded as a Number.
	 * @AttributeType Number
	 * @DefaultUnit Decimal degrees
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> windDirection;
	/**
	 * The observed wind speed in m/s, encoded as a Number.
	 * @AttributeType Number
	 * @DefaultUnit meters per second
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> windSpeed;
	/**
	 * The atmospheric pressure observed measured in Hecto Pascals.
	 * @AttributeType Number
	 * @DefaultUnit Hecto Pascals
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> atmosphericPressure;
	/**
	 *  Is the pressure rising or falling? It can be expressed in quantitative terms or qualitative terms.
	 * @AttributeType Text or Number
	 * @AttributeValues if expressed in quantitative terms: one Of (raising, falling, steady)
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> pressureTendency;
	/**
	 * The solar radiation observed measured in Watts per square meter.
	 * @AttributeType Number
	 * @DefaultUnit Watts per square meter
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> solarRadiation;
	/**
	 * TThe illumninance observed measured in lux (lx) or lumens per square metre (cd·sr·m−2).
	 * @AttributeType Number
	 * @DefaultUnit Lux
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> illuminance;
	
	public WeatherObserved() {
		super();
	}
	
	public WeatherObserved(String id) {
		super(id);
	}
	
	public NGSIAttribute<Date> getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(NGSIAttribute<Date> dateCreated) {
		this.dateCreated = dateCreated;
	}
	public NGSIAttribute<Date> getDateModified() {
		return dateModified;
	}
	public void setDateModified(NGSIAttribute<Date> dateModified) {
		this.dateModified = dateModified;
	}
	public Optional<NGSIAttribute<? extends GeoJSON<Object>>> getLocation() {
		return location;
	}
	public void setLocation(NGSIAttribute<? extends GeoJSON<Object>> location) {
		this.location = Optional.of(location);
	}
	public NGSIAttribute<String> getAddress() {
		return address;
	}
	public void setAddress(NGSIAttribute<String> address) {
		this.address = address;
	}
	public NGSIAttribute<String> getName() {
		return name;
	}
	public void setName(NGSIAttribute<String> name) {
		this.name = name;
	}
	public NGSIAttribute<Date> getDateObserved() {
		return dateObserved;
	}
	public void setDateObserved(NGSIAttribute<Date> dateObserved) {
		this.dateObserved = dateObserved;
	}
	public Optional<NGSIAttribute<String>> getSource() {
		return source;
	}
	public void setSource(NGSIAttribute<String> source) {
		this.source = Optional.of(source);
	}
	public Optional<NGSIAttribute<String>> getRefDevice() {
		return refDevice;
	}
	public void setRefDevice(NGSIAttribute<String> refDevice) {
		this.refDevice = Optional.of(refDevice);
	}
	public Optional<NGSIAttribute<String>> getRefPointOfInterest() {
		return refPointOfInterest;
	}
	public void setRefPointOfInterest(NGSIAttribute<String> refPointOfInterest) {
		this.refPointOfInterest = Optional.of(refPointOfInterest);
	}
	public Optional<NGSIAttribute<String>> getWeatherType() {
		return weatherType;
	}
	public void setWeatherType(NGSIAttribute<String> weatherType) {
		this.weatherType = Optional.of(weatherType);
	}
	public Optional<NGSIAttribute<Double>> getDewPoint() {
		return dewPoint;
	}
	public void setDewPoint(NGSIAttribute<Double> dewPoint) {
		this.dewPoint = Optional.of(dewPoint);
	}
	public Optional<NGSIAttribute<String>> getVisibility() {
		return visibility;
	}
	public void setVisibility(NGSIAttribute<String> visibility) {
		this.visibility = Optional.of(visibility);
	}
	public Optional<NGSIAttribute<Double>> getTemperature() {
		return temperature;
	}
	public void setTemperature(NGSIAttribute<Double> temperature) {
		this.temperature = Optional.of(temperature);
	}
	public Optional<NGSIAttribute<Double>> getRelativeHumidity() {
		return relativeHumidity;
	}
	public void setRelativeHumidity(NGSIAttribute<Double> relativeHumidity) {
		this.relativeHumidity = Optional.of(relativeHumidity);
	}
	public Optional<NGSIAttribute<Double>> getPrecipitation() {
		return precipitation;
	}
	public void setPrecipitation(NGSIAttribute<Double> precipitation) {
		this.precipitation = Optional.of(precipitation);
	}
	public Optional<NGSIAttribute<Double>> getWindDirection() {
		return windDirection;
	}
	public void setWindDirection(NGSIAttribute<Double> windDirection) {
		this.windDirection = Optional.of(windDirection);
	}
	public Optional<NGSIAttribute<Double>> getWindSpeed() {
		return windSpeed;
	}
	public void setWindSpeed(NGSIAttribute<Double> windSpeed) {
		this.windSpeed = Optional.of(windSpeed);
	}
	public Optional<NGSIAttribute<Double>> getAtmosphericPressure() {
		return atmosphericPressure;
	}
	public void setAtmosphericPressure(NGSIAttribute<Double> atmosphericPressure) {
		this.atmosphericPressure = Optional.of(atmosphericPressure);
	}
	public Optional<NGSIAttribute<String>> getPressureTendency() {
		return pressureTendency;
	}
	public void setPressureTendency(NGSIAttribute<String> pressureTendency) {
		this.pressureTendency = Optional.of(pressureTendency);
	}
	public Optional<NGSIAttribute<Double>> getSolarRadiation() {
		return solarRadiation;
	}
	public void setSolarRadiation(NGSIAttribute<Double> solarRadiation) {
		this.solarRadiation = Optional.of(solarRadiation);
	}
	public Optional<NGSIAttribute<Double>> getIlluminance() {
		return illuminance;
	}
	public void setIlluminance(NGSIAttribute<Double> illuminance) {
		this.illuminance = Optional.of(illuminance);
	}
	
	
	
	
}
