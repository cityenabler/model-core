package com.cityenabler.sdk.model.fiwaredatamodel;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.cityenabler.sdk.model.NGSIAttribute;
import com.cityenabler.sdk.model.NGSIEntity;
import com.cityenabler.sdk.model.type.GeoJSON;

public class PointOfInterest extends NGSIEntity {
	
	/** 
	 * Last update timestamp of this entity.
	 * @AttributeType DateTime
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateModified;
	/**
	 * Entity's creation timestamp.
	 * @AttributeType DateTime 
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateCreated;
	/**
	 * A sequence of characters giving the source of the entity data.
	 * @AttributeType Text or URL
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> source;
	/**
	 * Name of this point of interest.
	 * @NormativeReferences https://schema.org/name
	 * @Mandatory
	 */
	private NGSIAttribute<String> name;
	/**
	 * Alternative name for this point of interest.
	 * @NormativeReferences https://schema.org/alternateName
	 * @Mandatory
	 */
	private Optional<NGSIAttribute<String>> alternateName;
	/**
	 * Description of this point of interest.
	 * @NormativeReferences https://schema.org/description
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> description;
	/**
	 * Location of the point of interest represented by a GeoJSON geometry, usually a Point.
	 * @AttributeType geo:json
	 * @NormativeReferences https://tools.ietf.org/html/rfc7946
	 * @Mandatory If "address" is not defined
	 */
	private Optional<NGSIAttribute<? extends GeoJSON<Object>>> location;
	/**
	 * Civic address of this point of interest.
	 * @NormativeReferences https://schema.org/address
	 * @Mandatory If "location" is not defined
	 */
	private Optional<NGSIAttribute<String>> address;
	/**
	 * Category of this point of interest.
	 * @AttributeType List of Text
	 * @AllowedValue https://github.com/Factual/places/blob/master/categories/factual_taxonomy.json
	 * @Mandatory
	 */
	private NGSIAttribute<List<String>> category;
	/**
	 * Reference to one or more related entities that may provide extra, specific information about this point of interest.
	 * @AttributeType List of References
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> refSeeAlso;
	
	public PointOfInterest() {
		super();
	}
	
	public PointOfInterest(String id) {
		super(id);
	}
	
	public NGSIAttribute<Date> getDateCreated() {
		return dateCreated;
	}
	public Optional<NGSIAttribute<String>> getSource() {
		return source;
	}
	public void setSource(NGSIAttribute<String> source) {
		this.source = Optional.of(source);
	}
	public Optional<NGSIAttribute<String>> getDescription() {
		return description;
	}
	public void setDescription(NGSIAttribute<String> description) {
		this.description = Optional.of(description);
	}
	public Optional<NGSIAttribute<? extends GeoJSON<Object>>> getLocation() {
		return location;
	}
	public void setLocation(NGSIAttribute<? extends GeoJSON<Object>> location) {
		this.location = Optional.of(location);
	}
	public Optional<NGSIAttribute<String>> getAddress() {
		return address;
	}
	public void setAddress(NGSIAttribute<String> address) {
		this.address = Optional.of(address);
	}
	public NGSIAttribute<List<String>> getCategory() {
		return category;
	}
	public void setCategory(NGSIAttribute<List<String>> category) {
		this.category = category;
	}
	public Optional<NGSIAttribute<List<String>>> getRefSeeAlso() {
		return refSeeAlso;
	}
	public void setRefSeeAlso(NGSIAttribute<List<String>> refSeeAlso) {
		this.refSeeAlso = Optional.of(refSeeAlso);
	}
	public void setDateCreated(NGSIAttribute<Date> dateCreated) {
		this.dateCreated = dateCreated;
	}
	public NGSIAttribute<String> getName() {
		return name;
	}
	public void setName(NGSIAttribute<String> name) {
		this.name = name;
	}
	public Optional<NGSIAttribute<String>> getAlternateName() {
		return alternateName;
	}
	public void setAlternateName(NGSIAttribute<String> alternateName) {
		this.alternateName = Optional.of(alternateName);
	}
	public NGSIAttribute<Date> getDateModified() {
		return dateModified;
	}
	public void setDateModified(NGSIAttribute<Date> dateModified) {
		this.dateModified = dateModified;
	}
	
}
