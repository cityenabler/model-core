package com.cityenabler.sdk.model.fiwaredatamodel;

import java.util.Date;
import java.util.Optional;

import com.cityenabler.sdk.model.NGSIAttribute;
import com.cityenabler.sdk.model.NGSIEntity;
import com.cityenabler.sdk.model.type.GeoJSON;

public class TrafficFlowObserved extends NGSIEntity {
	/** 
	 * Last update timestamp of this entity.
	 * @AttributeType DateTime
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateModified;
	/**
	 * Entity's creation timestamp.
	 * @AttributeType DateTime 
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateCreated;
	/**
	 * Location of this traffic flow observation represented by a GeoJSON geometry.
	 * @AttributeType geo:json
	 * @NormativeReferences https://tools.ietf.org/html/rfc7946
	 * @Mandatory if refRoadSegment is not present.
	 */
	private Optional<NGSIAttribute<? extends GeoJSON<Object>>> location;
	/**
	 * Civic address of this traffic flow observation.
	 * @NormativeReferences https://schema.org/address
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> address;
	/**
	 * Concerned road segment on which the observation has been made.
	 * @AttributeType Reference to an entity of type RoadSegment
	 * @Mandatory if location is not present.
	 */
	private Optional<NGSIAttribute<String>> refRoadSegment;
	/**
	 * Concerned road segment on which the observation has been made.
	 * @AttributeType Number
	 * @AllowedValues Positive integer starting with 1. Lane identification is done using the conventions defined by RoadSegment which are based on OpenStreetMap.
	 * @Mandatory
	 */
	private NGSIAttribute<Double> laneId;
	/**
	 * The date and time of this observation in ISO8601 UTC format. It can be represented by an specific time instant or by an ISO8601 interval. As a workaround for the lack of support of Orion Context Broker for datetime intervals, it can be used two separate attributes: dateObservedFrom, dateObservedTo.
	 * @AttributeType DateTime
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateObserved;
	/**
	 * Observation period start date and time. See dateObserved.
	 * @AttributeType DateTime
	 * @Optional
	 */
	private Optional<NGSIAttribute<Date>> dateObservedFrom;
	/**
	 * Observation period end date and time. See dateObserved.
	 * @AttributeType DateTime
	 * @Optional
	 */
	private Optional<NGSIAttribute<Date>> dateObservedTo;
	/**
	 * Name given to this observation.
	 * @NormativeReferences https://schema.org/name
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> name;
	/**
	 * Description of this observation.
	 * @NormativeReferences https://schema.org/description
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> description;
	/**
	 * Total number of vehicles detected during this observation period.
	 * @AttributeType Number. Positive Integer.
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> intensity;
	/**
	 * Fraction of the observation time where a vehicle has been occupying the observed laned.
	 * @AttributeType Number between 0 and 1.
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> occupancy;
	/**
	 * Average speed of the vehicles transiting during the observation period.
	 * @AttributeType Number
	 * @DefaultUnit Kilometer per hour(Km/h)
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> averageVehicleSpeed;
	/**
	 * Average length of the vehicles transiting during the observation period.
	 * @AttributeType Number
	 * @DefaultUnit Meter(m)
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> averageVehicleLength;
	/**
	 * Flags whether there was a traffic congestion during the observation period in the referred lane. The absence of this attribute means no traffic congestion.
	 * @AttributeType Boolean
	 * @Optional
	 */
	private Optional<NGSIAttribute<Boolean>> congested;
	/**
	 * Average headway time. Headaway time is the time ellapsed between two consecutive vehicles.
	 * @AttributeType Number
	 * @DefaultUnit Second(s)
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> averageHeadwayTime;
	/**
	 * Average gap distance between consecutive vehicles.
	 * @AttributeType Number
	 * @DefaultUnit Meter(m)
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> averageGapDistance;
	/**
	 * Usual direction of travel in the lane referred by this observation. This attribute is useful when the observation is not referencing any road segment, allowing to know the direction of travel of the traffic flow observed.
	 * @AttributeType Text
	 * @AllowedValues  (forward, backward). See RoadSegment.laneUsage for a description of the semantics of these values
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> laneDirection;
	/**
	 * Flags whether traffic in the lane was reversed during the observation period. The absence of this attribute means no lane reversion.
	 * @AttributeType Boolean
	 * @Optional
	 */
	private Optional<NGSIAttribute<Boolean>> reversedLane;
	
	public TrafficFlowObserved() {
		super();
	}
	
	public TrafficFlowObserved(String id) {
		super(id);
	}
	
	public NGSIAttribute<Date> getDateModified() {
		return dateModified;
	}
	public void setDateModified(NGSIAttribute<Date> dateModified) {
		this.dateModified = dateModified;
	}
	public NGSIAttribute<Date> getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(NGSIAttribute<Date> dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Optional<NGSIAttribute<? extends GeoJSON<Object>>> getLocation() {
		return location;
	}
	public void setLocation(NGSIAttribute<? extends GeoJSON<Object>> location) {
		this.location = Optional.of(location);
	}
	public Optional<NGSIAttribute<String>> getAddress() {
		return address;
	}
	public void setAddress(NGSIAttribute<String> address) {
		this.address = Optional.of(address);
	}
	public Optional<NGSIAttribute<String>> getRefRoadSegment() {
		return refRoadSegment;
	}
	public void setRefRoadSegment(NGSIAttribute<String> refRoadSegment) {
		this.refRoadSegment = Optional.of(refRoadSegment);
	}
	public NGSIAttribute<Double> getLaneId() {
		return laneId;
	}
	public void setLaneId(NGSIAttribute<Double> laneId) {
		this.laneId = laneId;
	}
	public NGSIAttribute<Date> getDateObserved() {
		return dateObserved;
	}
	public void setDateObserved(NGSIAttribute<Date> dateObserved) {
		this.dateObserved = dateObserved;
	}
	public Optional<NGSIAttribute<Date>> getDateObservedFrom() {
		return dateObservedFrom;
	}
	public void setDateObservedFrom(NGSIAttribute<Date> dateObservedFrom) {
		this.dateObservedFrom = Optional.of(dateObservedFrom);
	}
	public Optional<NGSIAttribute<Date>> getDateObservedTo() {
		return dateObservedTo;
	}
	public void setDateObservedTo(NGSIAttribute<Date> dateObservedTo) {
		this.dateObservedTo = Optional.of(dateObservedTo);
	}
	public Optional<NGSIAttribute<String>> getName() {
		return name;
	}
	public void setName(NGSIAttribute<String> name) {
		this.name = Optional.of(name);
	}
	public Optional<NGSIAttribute<String>> getDescription() {
		return description;
	}
	public void setDescription(NGSIAttribute<String> description) {
		this.description = Optional.of(description);
	}
	public Optional<NGSIAttribute<String>> getIntensity() {
		return intensity;
	}
	public void setIntensity(NGSIAttribute<String> intensity) {
		this.intensity = Optional.of(intensity);
	}
	public Optional<NGSIAttribute<Double>> getOccupancy() {
		return occupancy;
	}
	public void setOccupancy(NGSIAttribute<Double> occupancy) {
		this.occupancy = Optional.of(occupancy);
	}
	public Optional<NGSIAttribute<Double>> getAverageVehicleSpeed() {
		return averageVehicleSpeed;
	}
	public void setAverageVehicleSpeed(NGSIAttribute<Double> averageVehicleSpeed) {
		this.averageVehicleSpeed = Optional.of(averageVehicleSpeed);
	}
	public Optional<NGSIAttribute<Double>> getAverageVehicleLength() {
		return averageVehicleLength;
	}
	public void setAverageVehicleLength(NGSIAttribute<Double> averageVehicleLength) {
		this.averageVehicleLength = Optional.of(averageVehicleLength);
	}
	public Optional<NGSIAttribute<Boolean>> getCongested() {
		return congested;
	}
	public void setCongested(NGSIAttribute<Boolean> congested) {
		this.congested = Optional.of(congested);
	}
	public Optional<NGSIAttribute<Double>> getAverageHeadwayTime() {
		return averageHeadwayTime;
	}
	public void setAverageHeadwayTime(NGSIAttribute<Double> averageHeadwayTime) {
		this.averageHeadwayTime = Optional.of(averageHeadwayTime);
	}
	public Optional<NGSIAttribute<Double>> getAverageGapDistance() {
		return averageGapDistance;
	}
	public void setAverageGapDistance(NGSIAttribute<Double> averageGapDistance) {
		this.averageGapDistance = Optional.of(averageGapDistance);
	}
	public Optional<NGSIAttribute<String>> getLaneDirection() {
		return laneDirection;
	}
	public void setLaneDirection(NGSIAttribute<String> laneDirection) {
		this.laneDirection = Optional.of(laneDirection);
	}
	public Optional<NGSIAttribute<Boolean>> getReversedLane() {
		return reversedLane;
	}
	public void setReversedLane(NGSIAttribute<Boolean> reversedLane) {
		this.reversedLane = Optional.of(reversedLane);
	}	
	
	
}
