package com.cityenabler.sdk.model.type.builder.fiware;

import com.cityenabler.sdk.model.NGSIType;
import com.cityenabler.sdk.model.type.builder.TypeBuilder;

public class NGSITypeBuilder extends TypeBuilder{
	
    @Override
	public String mapType(Object T) {
		String className = T.getClass().getSimpleName();
		String value = className;
		try {
			NGSIType type = NGSIType.valueOf(className);
			value = type.getTypeValue();
		} catch (IllegalArgumentException e) {
			//The ClassName is not part of the custom enumeration
		}
		return value;
	}

}
