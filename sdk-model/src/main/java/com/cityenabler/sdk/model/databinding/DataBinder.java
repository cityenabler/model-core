package com.cityenabler.sdk.model.databinding;

import com.cityenabler.sdk.model.NGSIObject;

public interface DataBinder {
	
	public void init();
	
	/**
	 * @param entity of type T
	 * @return the entity T serialized in json string
	 */
	public <T extends NGSIObject> String toJson(T entity);
	
	/**
	 * 
	 * @param json string
	 * @return the corresponding Object
	 */
	public <T extends NGSIObject> T toEntity(String json, Class<T> clazz);
}
