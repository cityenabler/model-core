package com.cityenabler.sdk.model.fiwaredatamodel;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.cityenabler.sdk.model.NGSIAttribute;
import com.cityenabler.sdk.model.NGSIEntity;
import com.cityenabler.sdk.model.type.GeoJSON;

public class WasteContainer extends NGSIEntity {
	/** 
	 * Last update timestamp of this entity.
	 * @AttributeType DateTime
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateModified;
	/**
	 * Entity's creation timestamp.
	 * @AttributeType DateTime 
	 * @Mandatory
	 */
	private NGSIAttribute<Date> dateCreated;
	/**
	 * Container's location represented by a GeoJSON Point
	 * @AttributeType geo:json
	 * @NormativeReferences https://tools.ietf.org/html/rfc7946
	 * @Mandatory If "address" is not defined
	 */
	private Optional<NGSIAttribute<? extends GeoJSON<Object>>> location;
	/**
	 *  Civic address where the container is located.
	 * @NormativeReferences https://schema.org/address
	 * @Mandatory If "location" is not defined
	 */
	private Optional<NGSIAttribute<String>> address;
	
	/**
	 * Filling level of the container (percentage, expressed in parts per one). When the container is full it must be equal to  1.0. When the container is empty it must be equal to 0.0. If it is not possible to determine the current filling level it must be equal to null.
	 * @AttributeType Number
	 * @AllowedValues Interval[0,1]
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> fillingLevel;
	/**
	 * Weight of the container load.
	 * @AttributeType Number
	 * @DefaultUnit Kilograms
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> cargoWeight;
	/**
	 * Temperature inside the container.
	 * @AttributeType Number
	 * @DefualtUnit Celsius Degrees
	 * @Optional
	 * 
	 */
	private Optional<NGSIAttribute<Double>> temperature;
	/**
	 * Methane (CH4) concentration inside the container
	 * @AttributeType Number
	 * @DefualtUnit Micrograms per cubic meter
	 * @Optional
	 * 
	 */
	private Optional<NGSIAttribute<Double>> methaneConcentration;
	/**
	 * Origin of the waste stored.
	 * @AttributeType Text
	 * @AllowedValues one Of (household, municipal, industrial, construction, hostelry, agriculture, other), or any other value which does not fit within the former.
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> storedWasteOrigin;
	
	/**
	 * Origin of the waste stored.
	 * @AttributeType List of Text
	 * @AllowedValues (organic, inorganic, glass, oil, plastic, metal, paper, batteries, electronics, hazardous, other) or any other value which does not fit within the former.
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> storedWasteKind;
	/**
	 * As per the regulation, waste codes which precisely identifies waste origin and kind.
	 * @AttributeType List of Text
	 * @AllowedValues Depend on the target regulation. For Europe, check Europe's List of Waste.
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> storedWasteCode;
	/**
	 * Container's model.
	 * @AttributeType Reference to a WasteContainerModel entity.
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> refWasteContainerModel;
	/**
	 * Serial number of the container.
	 * @NormativeReferences https://schema.org/serialNumber
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> serialNumber;
	/**
	 * Regulation under which the container is operating.
	 * @AttributeType List of Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> regulation;
	/**
	 * Responsible for the container, i.e. entity in charge of actuating (emptying, collecting etc.).
	 * @AttributeType Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> responsible;
	/**
	 * Container's owner
	 * @AttributeType Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> owner;
	/**
	 * Date at which the container started giving service.
	 * @AttributeType Date
	 * @Optional
	 */
	private Optional<NGSIAttribute<Date>> dateServiceStarted;
	/**
	 * Timestamp which represents when the container was emptied last time.
	 * @AttributeType DateTime
	 * @Optional
	 */
	private Optional<NGSIAttribute<Date>> dateLastEmptying;
	/**
	 * Deadline for next actuation to be performed (emptying, picking up, etc.).
	 * @AttributeType DateTime
	 * @Optional
	 */
	private Optional<NGSIAttribute<Date>> nextActuationDeadline;
	/**
	 * Hours suitable for performing actuations over the container.
	 * @AttributeType Text
	 * @NormativeReferences https://schema.org/openingHours
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> actuationHours;
	/**
	 * When the container was cleaned last time.
	 * @AttributeType DateTime
	 * @Optional
	 */
	private Optional<NGSIAttribute<Date>> dateLastCleaning;
	/**
	 * Deadline for next cleaning.
	 * @AttributeType DateTime
	 * @Optional
	 */
	private Optional<NGSIAttribute<Date>> nextCleaningDeadline;
	/**
	 * Isle where the container is placed.
	 * @AttributeType Reference to a WasteContainerIsle entity.
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> refWasteContainerIsle;
	/**
	 *  Identifier (or name) of the isle where the container is placed. This attribute should be used when entities of type  WasteContainerIsle are not being modelled specifically. Otherwise, refWasteContainerIsle should be used.
	 * @AttributeType Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> isleId;
	/**
	 * Container's category.
	 * @AttributeType List of Text
	 * @AllowedValues All values allowed for the category property of WasteContainerModel. fixed: Container is fixed to a wall, support or handle. "underground" Container is placed underground. 
	 * "ground" Container is placed at ground level. "portable" Container can be moved around a certain extent. 
	 * Any other value which captures an aspect not covered by those referred above.
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> category;
	/**
	 * Container's status from the point of view of safety.
	 * @AttributeType Text
	 * @AllowedValues "ok" Container is where it must be and stands properly.
	 * "lidOpen" Container's lid has been opened and not closed after a certain amount of time.
	 * "dropped" Container has been dropped for some reason.
	 * "moved" Container has been moved from its regular position and has not come back.
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> status;
	/**
	 * Container's color
	 * @AttributeType Text
	 * @AllowedValues A color keyword as specified by W3C Color Keywords or A color value as specified by W3C Color Data Type
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> color;
	/**
	 *  A URL containing a photo of the container.
	 *  @NormativeReferences https://schema.org/image
	 *  @Optional
	 */
	private Optional<NGSIAttribute<String>> image;
	/**
	 * Description about the container.
	 * @NormativeReferences https://schema.org/description
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> description;
	/**
	 * A field reserved for annotations (incidences, remarks, etc.).
	 * @AttributeType: List of Text
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> annotation;
	/**
	 *  Higher level area to which the container belongs to. It can be used to group containers per responsible, district, neighbourhood, etc.
	 *  @NormativeReferences https://pending.schema.org/areaServed
	 *  @Optional
	 */
	private Optional<NGSIAttribute<String>> areaServed;
	/**
	 * Timestamp saved by FIWARE's IoT Agent as per dynamic IoT data arrival. Note: This attribute has not been harmonized to keep backwards compatibility with current FIWARE reference implementations.
	 * @AttributeType DateTime
	 * @Optional
	 */
	private Optional<NGSIAttribute<Date>> TimeInstant;
	/**
	 * Reference to the device(s) used to monitor this container.
	 * @AttributeType List of Reference to entity(ies) of type Device
	 * @Optional
	 */
	private Optional<NGSIAttribute<List<String>>> refDevice;
	
	public WasteContainer(){
		super();
	}
	
	public WasteContainer(String id){
		super(id);
	}
	
	public NGSIAttribute<Date> getDateModified() {
		return dateModified;
	}
	public void setDateModified(NGSIAttribute<Date> dateModified) {
		this.dateModified = dateModified;
	}
	public NGSIAttribute<Date> getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(NGSIAttribute<Date> dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Optional<NGSIAttribute<? extends GeoJSON<Object>>> getLocation() {
		return location;
	}
	public void setLocation(NGSIAttribute<? extends GeoJSON<Object>> location) {
		this.location = Optional.of(location);
	}
	public Optional<NGSIAttribute<String>> getAddress() {
		return address;
	}
	public void setAddress(NGSIAttribute<String> address) {
		this.address = Optional.of(address);
	}
	public Optional<NGSIAttribute<Double>> getFillingLevel() {
		return fillingLevel;
	}
	public void setFillingLevel(NGSIAttribute<Double> fillingLevel) {
		this.fillingLevel = Optional.of(fillingLevel);
	}
	public Optional<NGSIAttribute<Double>> getCargoWeight() {
		return cargoWeight;
	}
	public void setCargoWeight(NGSIAttribute<Double> cargoWeight) {
		this.cargoWeight = Optional.of(cargoWeight);
	}
	public Optional<NGSIAttribute<Double>> getTemperature() {
		return temperature;
	}
	public void setTemperature(NGSIAttribute<Double> temperature) {
		this.temperature = Optional.of(temperature);
	}
	public Optional<NGSIAttribute<Double>> getMethaneConcentration() {
		return methaneConcentration;
	}
	public void setMethaneConcentration(NGSIAttribute<Double> methaneConcentration) {
		this.methaneConcentration = Optional.of(methaneConcentration);
	}
	public Optional<NGSIAttribute<String>> getStoredWasteOrigin() {
		return storedWasteOrigin;
	}
	public void setStoredWasteOrigin(NGSIAttribute<String> storedWasteOrigin) {
		this.storedWasteOrigin = Optional.of(storedWasteOrigin);
	}
	public Optional<NGSIAttribute<List<String>>> getStoredWasteKind() {
		return storedWasteKind;
	}
	public void setStoredWasteKind(NGSIAttribute<List<String>> storedWasteKind) {
		this.storedWasteKind = Optional.of(storedWasteKind);
	}
	public Optional<NGSIAttribute<List<String>>> getStoredWasteCode() {
		return storedWasteCode;
	}
	public void setStoredWasteCode(NGSIAttribute<List<String>> storedWasteCode) {
		this.storedWasteCode = Optional.of(storedWasteCode);
	}
	public Optional<NGSIAttribute<String>> getRefWasteContainerModel() {
		return refWasteContainerModel;
	}
	public void setRefWasteContainerModel(NGSIAttribute<String> refWasteContainerModel) {
		this.refWasteContainerModel = Optional.of(refWasteContainerModel);
	}
	public Optional<NGSIAttribute<String>> getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(NGSIAttribute<String> serialNumber) {
		this.serialNumber = Optional.of(serialNumber);
	}
	public Optional<NGSIAttribute<List<String>>> getRegulation() {
		return regulation;
	}
	public void setRegulation(NGSIAttribute<List<String>> regulation) {
		this.regulation = Optional.of(regulation);
	}
	public Optional<NGSIAttribute<String>> getResponsible() {
		return responsible;
	}
	public void setResponsible(NGSIAttribute<String> responsible) {
		this.responsible = Optional.of(responsible);
	}
	public Optional<NGSIAttribute<String>> getOwner() {
		return owner;
	}
	public void setOwner(NGSIAttribute<String> owner) {
		this.owner = Optional.of(owner);
	}
	public Optional<NGSIAttribute<Date>> getDateServiceStarted() {
		return dateServiceStarted;
	}
	public void setDateServiceStarted(NGSIAttribute<Date> dateServiceStarted) {
		this.dateServiceStarted = Optional.of(dateServiceStarted);
	}
	public Optional<NGSIAttribute<Date>> getDateLastEmptying() {
		return dateLastEmptying;
	}
	public void setDateLastEmptying(NGSIAttribute<Date> dateLastEmptying) {
		this.dateLastEmptying = Optional.of(dateLastEmptying);
	}
	public Optional<NGSIAttribute<Date>> getNextActuationDeadline() {
		return nextActuationDeadline;
	}
	public void setNextActuationDeadline(NGSIAttribute<Date> nextActuationDeadline) {
		this.nextActuationDeadline = Optional.of(nextActuationDeadline);
	}
	public Optional<NGSIAttribute<String>> getActuationHours() {
		return actuationHours;
	}
	public void setActuationHours(NGSIAttribute<String> actuationHours) {
		this.actuationHours = Optional.of(actuationHours);
	}
	public Optional<NGSIAttribute<Date>> getDateLastCleaning() {
		return dateLastCleaning;
	}
	public void setDateLastCleaning(NGSIAttribute<Date> dateLastCleaning) {
		this.dateLastCleaning = Optional.of(dateLastCleaning);
	}
	public Optional<NGSIAttribute<Date>> getNextCleaningDeadline() {
		return nextCleaningDeadline;
	}
	public void setNextCleaningDeadline(NGSIAttribute<Date> nextCleaningDeadline) {
		this.nextCleaningDeadline = Optional.of(nextCleaningDeadline);
	}
	public Optional<NGSIAttribute<String>> getRefWasteContainerIsle() {
		return refWasteContainerIsle;
	}
	public void setRefWasteContainerIsle(NGSIAttribute<String> refWasteContainerIsle) {
		this.refWasteContainerIsle = Optional.of(refWasteContainerIsle);
	}
	public Optional<NGSIAttribute<String>> getIsleId() {
		return isleId;
	}
	public void setIsleId(NGSIAttribute<String> isleId) {
		this.isleId = Optional.of(isleId);
	}
	public Optional<NGSIAttribute<List<String>>> getCategory() {
		return category;
	}
	public void setCategory(NGSIAttribute<List<String>> category) {
		this.category = Optional.of(category);
	}
	public Optional<NGSIAttribute<String>> getStatus() {
		return status;
	}
	public void setStatus(NGSIAttribute<String> status) {
		this.status = Optional.of(status);
	}
	public Optional<NGSIAttribute<String>> getColor() {
		return color;
	}
	public void setColor(NGSIAttribute<String> color) {
		this.color = Optional.of(color);
	}
	public Optional<NGSIAttribute<String>> getImage() {
		return image;
	}
	public void setImage(NGSIAttribute<String> image) {
		this.image = Optional.of(image);
	}
	public Optional<NGSIAttribute<String>> getDescription() {
		return description;
	}
	public void setDescription(NGSIAttribute<String> description) {
		this.description = Optional.of(description);
	}
	public Optional<NGSIAttribute<List<String>>> getAnnotation() {
		return annotation;
	}
	public void setAnnotation(NGSIAttribute<List<String>> annotation) {
		this.annotation = Optional.of(annotation);
	}
	public Optional<NGSIAttribute<String>> getAreaServed() {
		return areaServed;
	}
	public void setAreaServed(NGSIAttribute<String> areaServed) {
		this.areaServed = Optional.of(areaServed);
	}
	public Optional<NGSIAttribute<Date>> getTimeInstant() {
		return TimeInstant;
	}
	public void setTimeInstant(NGSIAttribute<Date> timeInstant) {
		TimeInstant = Optional.of(timeInstant);
	}
	public Optional<NGSIAttribute<List<String>>> getRefDevice() {
		return refDevice;
	}
	public void setRefDevice(NGSIAttribute<List<String>> refDevice) {
		this.refDevice = Optional.of(refDevice);
	}
	
	
	
}
