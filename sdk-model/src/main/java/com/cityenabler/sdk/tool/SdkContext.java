package com.cityenabler.sdk.tool;

import java.util.Optional;

import com.cityenabler.sdk.model.databinding.DataBinder;
import com.cityenabler.sdk.model.type.builder.NGSIAttributeTypeBuilder;
import com.cityenabler.sdk.model.type.builder.TypeBuilder;


public class SdkContext {
	   private static Optional<SdkContext> instance = Optional.empty();
	   private NGSIAttributeTypeBuilder typeBuilder = new TypeBuilder();
	   private DataBinder dataBinder = null;
	   
	   private SdkContext() {}
	   
	   public static SdkContext getIstance() {
		   if(!instance.isPresent())
			   instance = Optional.of(new SdkContext());
		   return instance.get();
	   }
	   
	   public NGSIAttributeTypeBuilder getTypeBuilder() {
		   return typeBuilder;
	   }
	   
	   public void setTypeBuilder(NGSIAttributeTypeBuilder tb) {
		   typeBuilder = tb;
	   }
	   
	   public DataBinder getDataBinder() {
		   return dataBinder;
	   }
	   
	   public void setDataBinder(DataBinder db) {
		   dataBinder = db;
	   }
}
