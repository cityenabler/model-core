package com.cityenabler.sdk.exception;

public class NoDataBinderDefinedException extends RuntimeException{
	private static final long serialVersionUID = 8402618696151312403L;
	private static String message = "No DataBinder definition found";
	
	public NoDataBinderDefinedException() {
		this(message);
	}
	
	public NoDataBinderDefinedException(String message) {
		super(message);
	}
}
