package com.cityenabler.sdk.serde.fiwaredatamodel;

import com.cityenabler.sdk.model.fiwaredatamodel.BikeHireDockingStation;
import com.cityenabler.sdk.serde.NgsiSerde;

public class BikeHireDockingStationSerde extends NgsiSerde<BikeHireDockingStation> {

}
