package com.cityenabler.sdk.serde;

import java.util.HashMap;
import java.util.Map;

import com.cityenabler.sdk.model.NGSIEntity;
import com.cityenabler.sdk.model.NGSIObject;
import com.cityenabler.sdk.model.NGSISubscription;
import com.cityenabler.sdk.model.fiwaredatamodel.AirQualityObserved;
import com.cityenabler.sdk.model.fiwaredatamodel.BikeHireDockingStation;
import com.cityenabler.sdk.model.fiwaredatamodel.GreenspaceRecord;
import com.cityenabler.sdk.model.fiwaredatamodel.KeyPerformanceIndicator;
import com.cityenabler.sdk.model.fiwaredatamodel.Museum;
import com.cityenabler.sdk.model.fiwaredatamodel.NoiseLevelObserved;
import com.cityenabler.sdk.model.fiwaredatamodel.OffStreetParking;
import com.cityenabler.sdk.model.fiwaredatamodel.ParkingSpot;
import com.cityenabler.sdk.model.fiwaredatamodel.PointOfInterest;
import com.cityenabler.sdk.model.fiwaredatamodel.TrafficFlowObserved;
import com.cityenabler.sdk.model.fiwaredatamodel.Vehicle;
import com.cityenabler.sdk.model.fiwaredatamodel.WasteContainer;
import com.cityenabler.sdk.model.fiwaredatamodel.WeatherObserved;
import com.cityenabler.sdk.serde.fiwaredatamodel.AirQualityObservedSerde;
import com.cityenabler.sdk.serde.fiwaredatamodel.BikeHireDockingStationSerde;
import com.cityenabler.sdk.serde.fiwaredatamodel.GreenspaceRecordSerde;
import com.cityenabler.sdk.serde.fiwaredatamodel.KeyPerformanceIndicatorSerde;
import com.cityenabler.sdk.serde.fiwaredatamodel.MuseumSerde;
import com.cityenabler.sdk.serde.fiwaredatamodel.NoiseLevelObservedSerde;
import com.cityenabler.sdk.serde.fiwaredatamodel.OffstreetParkingSerde;
import com.cityenabler.sdk.serde.fiwaredatamodel.ParkingSpotSerde;
import com.cityenabler.sdk.serde.fiwaredatamodel.PointOfInterestSerde;
import com.cityenabler.sdk.serde.fiwaredatamodel.TrafficFlowObservedSerde;
import com.cityenabler.sdk.serde.fiwaredatamodel.VehicleSerde;
import com.cityenabler.sdk.serde.fiwaredatamodel.WasteContainerSerde;
import com.cityenabler.sdk.serde.fiwaredatamodel.WeatherObservedSerde;

@SuppressWarnings({"unchecked", "rawtypes"})
public class SerdeDiscoveryService{
	
	//TODO make all the Serde auto-registering at startup
	
	private static Map<Class<? extends NGSIObject>, Class<? extends NgsiSerde>> serdes 
					= new HashMap<Class<? extends NGSIObject>, Class<? extends NgsiSerde>>(){{
						
						put(WasteContainer.class, WasteContainerSerde.class);
						put(AirQualityObserved.class, AirQualityObservedSerde.class);
						put(BikeHireDockingStation.class, BikeHireDockingStationSerde.class);
						put(GreenspaceRecord.class, GreenspaceRecordSerde.class);
						put(KeyPerformanceIndicator.class, KeyPerformanceIndicatorSerde.class);
						put(Museum.class, MuseumSerde.class);
						put(NoiseLevelObserved.class, NoiseLevelObservedSerde.class);
						put(OffStreetParking.class, OffstreetParkingSerde.class);
						put(ParkingSpot.class, ParkingSpotSerde.class);
						put(PointOfInterest.class, PointOfInterestSerde.class);
						put(TrafficFlowObserved.class, TrafficFlowObservedSerde.class);
						put(Vehicle.class, VehicleSerde.class);
						put(WeatherObserved.class, WeatherObservedSerde.class);
						
						put(NGSISubscription.class,NGSISubscriptionSerde.class);
					}};
	
	public static <T extends NGSIObject> NgsiSerde<T> findForClass(Class<T> entityclass){
		try { return (NgsiSerde<T>) serdes.get(entityclass).getConstructor().newInstance(); }
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static <T extends NGSIObject> NgsiSerde<T> findFor(T entity){
		return (NgsiSerde<T>) findForClass(entity.getClass());
	}
	
	public static <T extends NGSIObject> void registerSerde(Class<T> entityclass, Class<? extends NgsiSerde> serdeclass){
		serdes.put(entityclass, serdeclass);
	}
	
}