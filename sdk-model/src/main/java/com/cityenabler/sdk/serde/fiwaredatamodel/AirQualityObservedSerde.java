package com.cityenabler.sdk.serde.fiwaredatamodel;

import com.cityenabler.sdk.model.fiwaredatamodel.AirQualityObserved;
import com.cityenabler.sdk.serde.NgsiSerde;
import com.cityenabler.sdk.serde.SerdeDiscoveryService;

public class AirQualityObservedSerde extends NgsiSerde<AirQualityObserved> {
}
