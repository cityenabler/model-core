package com.cityenabler.sdk.serde;

import java.lang.reflect.ParameterizedType;
import java.util.Optional;

import com.cityenabler.sdk.exception.NoDataBinderDefinedException;
import com.cityenabler.sdk.model.NGSIEntity;
import com.cityenabler.sdk.model.NGSIObject;
import com.cityenabler.sdk.model.databinding.DataBinder;
import com.cityenabler.sdk.tool.SdkContext;

public abstract class NgsiSerde<T extends NGSIObject> implements ISerde<T>{
	
	private DataBinder dataBinder;
	
	public NgsiSerde() {
		this.dataBinder = SdkContext.getIstance().getDataBinder();
		
		if(dataBinder == null) {
			throw new NoDataBinderDefinedException();
		}
		
		this.dataBinder.init();
		
	}
	
	@SuppressWarnings("unchecked")
	private Class<T> getActualEntityType(){
		return (Class<T>)((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public String toJson(T entity) {
		String json = "";

		if(dataBinder == null) {
			throw new NoDataBinderDefinedException();
		}			
		json = this.dataBinder.toJson(entity);

		return json;
	}
	
	public T toEntity(String json) {
		
		Class<T> clazz = getActualEntityType();
		Optional<T> out;
		try{
			//Set the default Optional to an empty FIWARE Datamodel
			out = Optional.of(clazz.getConstructor().newInstance(new Object[]{})); 
		}
		catch(Exception e1){
			//In case of problems in the creation of the empty FIWARE Datamodel, continue with an empty Optional
			out = Optional.empty();
		}
		
		T d = null;
		

		if(dataBinder == null) {
			throw new NoDataBinderDefinedException();
		}			
		
		d = (T) this.dataBinder.toEntity(json, clazz);

		if(d != null) 
			out = Optional.of(d); 
		
		//If the Optional has a value, returns it; otherwise returns null
		return out.orElse(null);
	}
}
