package com.cityenabler.sdk.serde.fiwaredatamodel;

import com.cityenabler.sdk.model.fiwaredatamodel.TrafficFlowObserved;
import com.cityenabler.sdk.serde.NgsiSerde;

public class TrafficFlowObservedSerde extends NgsiSerde<TrafficFlowObserved> {

}
