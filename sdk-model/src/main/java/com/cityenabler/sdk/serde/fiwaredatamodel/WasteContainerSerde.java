package com.cityenabler.sdk.serde.fiwaredatamodel;

import com.cityenabler.sdk.model.fiwaredatamodel.WasteContainer;
import com.cityenabler.sdk.serde.NgsiSerde;

public class WasteContainerSerde extends NgsiSerde<WasteContainer> {
}
