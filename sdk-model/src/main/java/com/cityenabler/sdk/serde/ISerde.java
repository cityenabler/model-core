package com.cityenabler.sdk.serde;

public interface ISerde <T> {

	/**
	 * @param entity of type T
	 * @return the entity T serialized in json string
	 */
	public String toJson(T entity);
	
	/**
	 * 
	 * @param json string
	 * @return the corresponding NGSIEntity Object
	 */
	public T toEntity(String json);
}
