package com.cityenabler.sdk.tests;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import com.cityenabler.sdk.model.TestEntity;
import com.cityenabler.sdk.model.TestEntitySerde;
import com.cityenabler.sdk.model.TestEntity.TestValue;
import com.cityenabler.sdk.model.databinding.DefaultDataBinder;
import com.cityenabler.sdk.model.type.builder.fiware.NGSITypeBuilder;
import com.cityenabler.sdk.tool.SdkContext;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

@SuppressWarnings("deprecation")
public class ObjectMappingTest {
	
	private static TestEntitySerde serde = null;
	
	@BeforeClass 
	public static void setUpSerde() {
		SdkContext.getIstance().setDataBinder(new DefaultDataBinder());
		SdkContext.getIstance().setTypeBuilder(new NGSITypeBuilder());

		serde = new TestEntitySerde();
	}
	
	private Date getInstant(){
		try{
			ISO8601DateFormat sdf = new ISO8601DateFormat();
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			Date d = sdf.parse("2018-06-18T15:51:26Z");
			return d;
		}
		catch(Exception e){
			e.printStackTrace();
			return GregorianCalendar.getInstance().getTime();
		}
	}
	
	private String getTestentityJSON(){
		String result = "";
		ClassLoader classLoader = getClass().getClassLoader();
		try {
		    result = IOUtils.toString(classLoader.getResourceAsStream("testentity.json"));
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
			
		return result;
	}
	
	private TestEntity getTestEntityMock(){
		Date instant = getInstant();
		TestValue tmp = new TestEntity.TestValue(4.34F, 5.34F);
		
		TestEntity test = new TestEntity("ninotest", "TestEntity");
		test.setAttribute("counter", 1);
		test.setAttribute("dateObserved", instant);
		test.setAttribute("name", "Nino Test name");
		test.setAttribute("measure", tmp);
		test.getMeasure().addMetadata("TimeInstant", instant);
		
		return test;
	}
	
	@Test
	public void ngsi2Json() {
		TestEntity test = getTestEntityMock();
		try {
			String s = serde.toJson(test);
			assertTrue(getTestentityJSON().equalsIgnoreCase(s));
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}
	
	@Test
	public void json2Ngsi() {
		try {
			TestEntity entity = serde.toEntity(getTestentityJSON());
			assertTrue(getTestEntityMock().equals(entity));
		} 
		catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
	}

	@Test
	public void json2json() {
		try {
			String s = getTestentityJSON();
			TestEntity entity = serde.toEntity(s);
			String s2 = serde.toJson(entity);
			assertTrue(s.equals(s2));
		} 
		catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
	}
	
	@Test
	public void ngsi2ngsi() {
		TestEntity test = getTestEntityMock();
		try {
			TestEntity test2 = serde.toEntity(serde.toJson(test));
			assertTrue(test.equals(test2));
		} 
		catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}
	
}
