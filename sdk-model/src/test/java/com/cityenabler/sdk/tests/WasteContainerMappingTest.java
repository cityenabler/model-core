package com.cityenabler.sdk.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import com.cityenabler.sdk.model.NGSIAttribute;
import com.cityenabler.sdk.model.databinding.DefaultDataBinder;
import com.cityenabler.sdk.model.fiwaredatamodel.WasteContainer;
import com.cityenabler.sdk.model.type.builder.fiware.NGSITypeBuilder;
import com.cityenabler.sdk.model.type.geojson.GeoPolygon;
import com.cityenabler.sdk.serde.fiwaredatamodel.WasteContainerSerde;
import com.cityenabler.sdk.serde.NgsiSerde;
import com.cityenabler.sdk.serde.SerdeDiscoveryService;
import com.cityenabler.sdk.tool.SdkContext;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

@SuppressWarnings("deprecation")
public class WasteContainerMappingTest {
	
	private static NgsiSerde<WasteContainer> serde = null;
	
	@BeforeClass 
	public static void setUpSerde() {
		SdkContext.getIstance().setDataBinder(new DefaultDataBinder());
		SdkContext.getIstance().setTypeBuilder(new NGSITypeBuilder());

		serde = new WasteContainerSerde();
	}
	
	private Date getInstant(){
		try{
			ISO8601DateFormat sdf = new ISO8601DateFormat();
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			Date d = sdf.parse("2018-06-18T15:51:26Z");
			return d;
		}
		catch(Exception e){
			e.printStackTrace();
			return GregorianCalendar.getInstance().getTime();
		}
	}
	
	private String getTestentityJSON(){
		String result = "";
		ClassLoader classLoader = getClass().getClassLoader();
		try {
		    result = IOUtils.toString(classLoader.getResourceAsStream("wasteentity.json"));
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
			
		return result;
	}
	
	private WasteContainer getTestEntityMock(){
		Date instant = getInstant();
		

		WasteContainer test = new WasteContainer("ninowastetest");
	
		test.setAttribute("dateModified", instant);
		test.setAttribute("isleId", "idIsola");
		
		Optional<NGSIAttribute<String>> isleid = test.getIsleId();
		if(isleid.isPresent())
			isleid.get().addMetadata("TimeInstant", instant);
		
		//GeoPoint location = new GeoPoint(51.151513,0.56421);
		List<List<List<Double>>> coordinates= new ArrayList<List<List<Double>>>();
		List<List<Double>> segment = new ArrayList<List<Double>>();
		List<Double> vertex = Arrays.asList(1512666.5987,5035205.591499999);
		List<Double> vertex2 = Arrays.asList(1512666.5987,5035205.591499999);
		segment.add(vertex);
		segment.add(vertex2);
		coordinates.add(segment);
		GeoPolygon location = new GeoPolygon(coordinates);
		
		test.setAttribute("location", location);
		test.setAttribute("storedWasteKind", Arrays.asList("organic","glass","oil"));
		
		return test;
	}
	
	@Test
	public void ngsi2Json() {
		WasteContainer test = getTestEntityMock();
		try {
			if(serde==null)
				serde = SerdeDiscoveryService.findFor(test);
			String s = serde.toJson(test);
			assertTrue(getTestentityJSON().equalsIgnoreCase(s));
			
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}
	
	@Test
	public void json2Ngsi() {
		try {
			if(serde==null)
				serde = SerdeDiscoveryService.findForClass(WasteContainer.class);
			WasteContainer entity = serde.toEntity(getTestentityJSON());
			assertTrue(getTestEntityMock().equals(entity));
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Test
	public void json2json() {
		try {
			if(serde==null)
				serde = SerdeDiscoveryService.findForClass(WasteContainer.class);
			String s = getTestentityJSON();
			WasteContainer entity = serde.toEntity(s);
			assertTrue(s.equals(serde.toJson(entity)));
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void ngsi2ngsi() {
		WasteContainer test = getTestEntityMock();
		try {
			if(serde==null)
				serde = SerdeDiscoveryService.findFor(test);
			WasteContainer test2 = serde.toEntity(serde.toJson(test));
			assertTrue(test.equals(test2));
		} 
		catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}
	
	@Test
	public void testEmptyEntity() {
		WasteContainer test = new WasteContainer();
		System.out.println(test.getId());
	}
	
}
