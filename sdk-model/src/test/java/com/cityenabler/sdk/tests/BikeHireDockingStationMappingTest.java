package com.cityenabler.sdk.tests;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import com.cityenabler.sdk.model.databinding.DefaultDataBinder;
import com.cityenabler.sdk.model.fiwaredatamodel.BikeHireDockingStation;
import com.cityenabler.sdk.model.type.builder.fiware.NGSITypeBuilder;
import com.cityenabler.sdk.model.type.geojson.GeoPoint;
import com.cityenabler.sdk.serde.fiwaredatamodel.BikeHireDockingStationSerde;
import com.cityenabler.sdk.tool.SdkContext;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

@SuppressWarnings("deprecation")
public class BikeHireDockingStationMappingTest {
	
	private static BikeHireDockingStationSerde serde = null;
	
	@BeforeClass 
	public static void setUpSerde() {
		SdkContext.getIstance().setDataBinder(new DefaultDataBinder());
		SdkContext.getIstance().setTypeBuilder(new NGSITypeBuilder());

		serde = new BikeHireDockingStationSerde();
	}
	
	private Date getInstant(){
		try{
			ISO8601DateFormat sdf = new ISO8601DateFormat();
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			Date d = sdf.parse("2018-06-17T15:51:26Z");
			return d;
		}
		catch(Exception e){
			e.printStackTrace();
			return GregorianCalendar.getInstance().getTime();
		}
	}
	
	private String getTestentityJSON(){
		String result = "";
		ClassLoader classLoader = getClass().getClassLoader();
		try {
		    result = IOUtils.toString(classLoader.getResourceAsStream("bikehiredockingstationentity.json"));
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
			
		return result;
	}
	
	private BikeHireDockingStation getTestEntityMock(){
		Date instant = getInstant();
		
		BikeHireDockingStation test = new BikeHireDockingStation("bikehiredockingstation_test");
	
		test.setAttribute("dateCreated", instant);
		test.setAttribute("dateModified", instant);
		
		GeoPoint location = new GeoPoint(51.151513,0.56421);
	
		test.setAttribute("location", location);
		test.setAttribute("name", "BK_SLOT_1");
		test.setAttribute("totalSlotNumber", 19);
		test.setAttribute("availableBikeNumber", 12);
		test.setAttribute("status", Arrays.asList("working","almostEmpty"));
		test.setAttribute("owner", "Danilo Trombino");
		return test;
	}
	
	@Test
	public void ngsi2Json() {
		BikeHireDockingStation test = getTestEntityMock();
		try {
			String s = serde.toJson(test);
			assertTrue(getTestentityJSON().equalsIgnoreCase(s));
			
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}
	
	@Test
	public void json2Ngsi() {
		try {
			BikeHireDockingStation entity = serde.toEntity(getTestentityJSON());
			assertTrue(getTestEntityMock().equals(entity));
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Test
	public void json2json() {
		try {
			String s = getTestentityJSON();
			BikeHireDockingStation entity = serde.toEntity(s);
			assertTrue(s.equals(serde.toJson(entity)));
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void ngsi2ngsi() {
		BikeHireDockingStation test = getTestEntityMock();
		try {
			BikeHireDockingStation test2 = serde.toEntity(serde.toJson(test));
			assertTrue(test.equals(test2));
		} 
		catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}
	
	@Test
	public void testEmptyEntity() {
		BikeHireDockingStation test = new BikeHireDockingStation();
		System.out.println(test.getId());
	}
	
}
