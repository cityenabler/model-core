package com.cityenabler.sdk.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ObjectMappingTest.class, WasteContainerMappingTest.class})
public class AllTests {

	@Test
	public void json2NGSIObj() {
		assertTrue(true);
	}
	
}
