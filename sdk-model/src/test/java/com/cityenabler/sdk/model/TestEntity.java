package com.cityenabler.sdk.model;

import java.util.Date;

public class TestEntity extends NGSIEntity{
	
	public static class TestValue{
		private Float value;
		private Float value2;
		
		public TestValue() {
			this.value = 0.0F;
			this.value2 = 0.0F;
		}
		
		public TestValue(Float value, Float value2) {
			this.value = value;
			this.setValue2(value2);
		}
		
		public Float getValue() {
			return value;
		}
		public void setValue(Float value) {
			this.value = value;
		}

		public Float getValue2() {
			return value2;
		}

		public void setValue2(Float value2) {
			this.value2 = value2;
		}
		
	}

	private NGSIAttribute<String> name;
	private NGSIAttribute<Integer> counter;
	private NGSIAttribute<TestValue> measure;
	private NGSIAttribute<Date> dateObserved;
	
	public TestEntity() {
		super();
	}
	
	public TestEntity(String id, String type) {
		super(id, type);
	}

	public NGSIAttribute<String> getName() {
		return name;
	}

	public NGSIAttribute<Integer> getCounter() {
		return counter;
	}

	public NGSIAttribute<TestValue> getMeasure() {
		return measure;
	}

	public NGSIAttribute<Date> getDateObserved() {
		return dateObserved;
	}

	public void setName(NGSIAttribute<String> name) {
		this.name = name;
	}

	public void setCounter(NGSIAttribute<Integer> counter) {
		this.counter = counter;
	}

	public void setMeasure(NGSIAttribute<TestValue> measure) {
		this.measure = measure;
	}

	public void setDateObserved(NGSIAttribute<Date> dateObserved) {
		this.dateObserved = dateObserved;
	}

}
